## 文档说明

`GET`请求请以`params`方式组装参数，也就是组装到`url`,示例如下:

```javascript
export function getUserList (parameter) {
  return axios({
    url: api.user + '/list',
    method: 'get',
    params: parameter
  })
}
```

`POST`请求根据`Content-Type`来使用`data`或者`params`,示例如下:

```javascript

//'Content-Type': 'application/json'
export function changUserStatus (parameter) {
  return axios({
    url: api.user + '/status',
    method: 'post',
    data: parameter
  })
}
//'Content-Type':'application/x-www-form-urlencoded'
export function delUser (parameter) {
  return axios({
    url: api.user + '/remove',
    method: 'post',
    params: parameter
  })
}
```

