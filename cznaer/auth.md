# 授权

## 获取图形验证码
**URL:** http://{server}/captcha/captchaImage

**Type:** GET

**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
type|string|验证码类型，只支持 math 算术(默认) 和 char 字符串|否

**请求示例:**

```
http://{server}/captcha/captchaImage?type=math
```

**响应header:**

字段 | 类型|描述
---|---|---
randmomstr|string|验证码的编号，提交时需用到

返回时`header`会带有`randmomstr` 是该验证码的编号，提交时需用到,`content-type image/jpeg` 是图片二进制流，需要自行解析

## 登陆
**URL:** http://{server}/login

**Type:** POST

**Content-Type:** application/json


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
username|string|用户名|<font color=red>是</font>
password|string|密码|<font color=red>是</font>
captcha|string|验证码|<font color=red>是</font>
randomstr|string|验证码编号|<font color=red>是</font>

**请求示例:**
```json
{
    "username": "admin",
    "password": "admin123",
    "captcha": "15",
    "randomstr": "5ba646e000b842eaa891ed03191a5ea6"
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
expire|int|有效时长（秒）,客户端可以低于这个时间，但不能超出
userId|string|用户编号
token|string|令牌


**返回示例:**
```json
{
    "msg": "success",
    "code": 0,
    "expire": 43200,
    "userId": 1,
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NjQ0MzQ4NDAsInVzZXJuYW1lIjoiYWRtaW4ifQ.mbCT-MeoIAHkAsyAeRhL0MC1l0DoJmZaCeyW217Bm4I"
}
```

## 获取用户信息
**URL:** http://{server}/info

**Type:** GET

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌

**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
buttons|array|按钮权限
menus|array|菜单
user|obj|用户信息

**返回示例:**

```json
{
    "msg": "success",
    "code": 0,
    "buttons": [],
    "menus": [
        {
            "remark": null,
            "params": {},
            "menuId": 1,
            "menuName": "系统管理",
            "parentName": null,
            "parentId": 0,
            "orderNum": "6",
            "url": "#",
            "menuType": "M",
            "visible": null,
            "perms": "",
            "icon": "fa fa-gear",
            "children": [
                {
                    "searchValue": null,
                    "createBy": null,
                    "createTime": "2018-03-16 11:33:00",
                    "updateBy": null,
                    "updateTime": null,
                    "remark": null,
                    "params": {},
                    "menuId": 108,
                    "menuName": "日志管理",
                    "parentName": null,
                    "parentId": 1,
                    "orderNum": "9",
                    "url": "#",
                    "menuType": "M",
                    "visible": null,
                    "perms": "",
                    "icon": "#",
                    "children": [
                        {
                            "remark": null,
                            "params": {},
                            "menuId": 500,
                            "menuName": "操作日志",
                            "parentName": null,
                            "parentId": 108,
                            "orderNum": "1",
                            "url": "/monitor/operlog",
                            "menuType": "C",
                            "visible": null,
                            "perms": "monitor:operlog:view",
                            "icon": "#",
                            "children": []
                        },...
                    ]
                }
            ]
        },...
        
    ],
    "user": {
        "remark": "管理员",
        "params": {},
        "userId": 1,
        "deptId": 103,
        "parentId": null,
        "loginName": "admin",
        "userName": "车在哪儿",
        "email": "18926042326@qq.com",
        "phonenumber": "18926042326",
        "sex": "0",
        "avatar": "e36565de102c13ab9581fdba2b52ab0c.jpg",
        "password": "29c67a30398638269fe600f73a054934",
        "salt": "111111",
        "status": "0",
        "delFlag": "0",
        "loginIp": "127.0.0.1",
        "loginDate": "2019-07-29 17:14:00",
        "dept": {
            "remark": null,
            "params": {},
            "deptId": 103,
            "parentId": 101,
            "ancestors": null,
            "deptName": "研发部门",
            "orderNum": "1",
            "leader": null,
            "phone": null,
            "email": null,
            "status": "0",
            "delFlag": null,
            "parentName": null
        },
        "roles": [
            {
                "params": {},
                "roleId": 1,
                "roleName": "管理员",
                "roleKey": "admin",
                "roleSort": "1",
                "dataScope": "1",
                "status": "0",
                "delFlag": null,
                "flag": false,
                "menuIds": null,
                "deptIds": null
            }
        ],
        "roleIds": null,
        "postIds": null,
        "admin": true
    }
}
```
## 登出
**URL:** http://{server}/logout

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

| Name  | Type   | Description |
| ----- | ------ | ----------- |
| token | string | 授权令牌    |

**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```json
{
    "msg": "success",
    "code": 0
}
```


