# 部门管理

## 部门查询列表
**URL:** http://{server}/sys/dept/list

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
deptId|Integer|部门id|否
parentId|Integer|父部门id|否
ancestors|String|祖级列表|否
deptName|String|部门名称|否
orderNum|Integer|显示顺序|否
leader|String|负责人|否
phone|String|联系电话|否
email|String|邮箱|否
status|String|部门状态（0正常 1停用）|否
delFlag|String|删除标志（0代表存在 2代表删除）|否
pageNum|int|页码|否
pageSize|int|行数|否
params.beginTime|Date|开始时间|否
params.endTime|Date|结束时间|否

**请求示例:**

```json
{
	"deptId":Integer,
	"parentId":Integer,
	"ancestors":String,
	"deptName":String,
	"orderNum":Integer,
	"leader":String,
	"phone":String,
	"email":String,
	"status":String,
	"delFlag":String,
	"params.beginTime":"2019-01-01 00:00:00",
	"params.endTime":"2029-01-01 00:00:00"
}

```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
rows|array|集合
total|number|记录总数


**返回示例:**
```json
{
	"code":0,
	"msg":"success",
	"total":100,
	"rows":{}
}
```

## 部门根据编号查询
**URL:** http://{server}/sys/dept/get/{deptId}

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
deptId|Integer|主键|是

**请求示例:**
```
http://{server}/sys/dept/get/123
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
data|object|对象


**返回示例:**
```
{
	"code":0,
	"msg":"success",
	"data":{
		"deptId":Integer,
		"parentId":Integer,
		"ancestors":String,
		"deptName":String,
		"orderNum":Integer,
		"leader":String,
		"phone":String,
		"email":String,
		"status":String,
		"delFlag":String,
		"createBy":String,
		"createTime":Date,
		"updateBy":String,
		"updateTime":Date
	}
}
```

## 部门新增
**URL:** http://{server}/sys/dept/add

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
deptId|Integer|部门id|否
parentId|Integer|父部门id|否
ancestors|String|祖级列表|否
deptName|String|部门名称|否
orderNum|Integer|显示顺序|否
leader|String|负责人|否
phone|String|联系电话|否
email|String|邮箱|否
status|String|部门状态（0正常 1停用）|否
delFlag|String|删除标志（0代表存在 2代表删除）|否

**请求示例:**
```
{
	"deptId":Integer,
	"parentId":Integer,
	"ancestors":String,
	"deptName":String,
	"orderNum":Integer,
	"leader":String,
	"phone":String,
	"email":String,
	"status":String,
	"delFlag":String,
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```

## 部门更新
**URL:** http://{server}/sys/dept/edit

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
deptId|Integer|部门id|否
parentId|Integer|父部门id|否
ancestors|String|祖级列表|否
deptName|String|部门名称|否
orderNum|Integer|显示顺序|否
leader|String|负责人|否
phone|String|联系电话|否
email|String|邮箱|否
status|String|部门状态（0正常 1停用）|否
delFlag|String|删除标志（0代表存在 2代表删除）|否

**请求示例:**
```
{
	"deptId":Integer,
	"parentId":Integer,
	"ancestors":String,
	"deptName":String,
	"orderNum":Integer,
	"leader":String,
	"phone":String,
	"email":String,
	"status":String,
	"delFlag":String,
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```

## 部门删除
**URL:** http://{server}/sys/dept/remove

**Type:** POST

**Content-Type:** application/x-www-form-urlencoded

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
ids|string|id字符串如"1,2,3"|是

**请求示例:**
```
url: 'http://{server}/sys/dept/remove',
method: 'post',
data: {ids:'1,2,3'}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```