# 设备轨迹

##  根据设备号查询轨迹
**URL:** http://{server}/biz/devdata/list

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
equnum|String|设备号|<font color="red">是</font>
params.beginTime|Date|开始时间|<font color="red">是</font>
params.endTime|Date|结束时间|<font color="red">是</font>

**说明**：会校验设备拥有者和查询者组织关系

**请求示例:**

```json
{
	"equnum":"123",
	"params.beginTime":"2019-01-01 00:00:00",
	"params.endTime":"2029-01-01 00:00:00"
}

```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
rows|array|集合
total|number|记录总数


**返回示例:**
```json
{
    "code": 0,
    "msg": "success",
    "total": 100,
    "rows": [
        {
            "ln": 113.843456217448,//经度
            "address": "广东省深圳市宝安区西乡街道固戍二路103",//地址
            "equnum": "5207323374",//设备编号
            "data_id": 93150,//GPS数据编号
            "la": 22.598774414063,//纬度
            "data_time": 1557482707556,//GPS数据时间
            "id": 87663,//数据编号
            "speed": 0,//速度 km/h
            "direction": 0,//方向角
            "device_time": 1557482708000//设备上传时间
        },...
    ]
}
```

