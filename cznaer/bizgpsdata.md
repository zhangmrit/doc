# 车辆数据

## 车辆数据查询列表
**URL:** http://{server}/biz/devdata/list

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
id|Integer||否
datatype|Integer|1:开机,11:GPS,12:LBS定位,13:GPS双温油|否
datahead|String|数据长度|否
sn|String|序列号|否
ondevicecount|String|开机次数|否
ararmstate|String|报警位|否
recordnum|String|该条记录的记录号|否
deviceid|String|设备号|否
devicetime|Long|GPS时间|否
devicedate|String|GPS日期|否
longitude|String|原始经度|否
latitude|String|原始纬度|否
tempone|String|温度值1 修改20180403  包含了正负|否
temptwo|String|温度值2 修改20180403  包含了正负|否
temponestate|String|温度值1的状态  这个属性不需要了，以前为了保存温度的正负|否
temptwostate|String|温度值2的状态  这个属性不需要了，以前为了保存温度的正负|否
speedandirection|String|速度、方向   拆分为两个speed和direction|否
locationstate|String|定位状态|否
power|String|电量|否
productid|String|产品编号|否
protocolnum|String|协议号|否
responsetype|String|应答类型|否
mnc|String|MNC|否
mcc|String|MCC|否
lacone|String|LOC_1|否
cione|String|CI_1|否
lactwo|String|LOC_2|否
citwo|String|CI_2|否
lacthr|String|LOC_3|否
cithr|String|CI_3|否
oilone|String|油量1|否
oiltwo|String|油量2|否
datalength|String|补充字段|否
mileage|String|里程数|否
createtime|Long|创建时间|否
del|Integer|删除状态|否
speed|String|速度|否
direction|String|方向，就是方位角。|否
deviceState|String|设备的工作模式|否
fzeropostion|String|第一字节第一位  温度报警|否
fonepostion|String|三次密码错误报警|否
ftwopostion|String|GPRS阻塞报警|否
fthreepostion|String|断油电状态/ 1连接0断开|否
ffourpostion|String|本机设备拆除报警|否
ffivepostion|String|反转高电平 0为高|否
fsixpostion|String|正转高电平 0为高|否
fsevenpostion|String|第一字节第七位  低电平传感器1搭铁|否
szeropostion|String|第二字节第一位  GPS接收机故障报警|否
sonepostion|String|子母机状态/ 1母机0子机|否
stwopostion|String|LBS状态/ 1打开0关闭|否
sthreepostion|String|主机由后备电池供电|否
sfourpostion|String|外部电源状态/ 1正常0拆除|否
sfivepostion|String|GPS天线开路|否
ssixpostion|String|GPS天线短路|否
ssevenpostion|String|第二字节第七位 低电平传感器2搭铁|否
tzeropostion|String|第三字节第一位 车门开|否
tonepostion|String|车辆设防|否
ttwopostion|String|ACC状态/1点火0熄火|否
tthreepostion|String|母机拆除报警|否
tfourpostion|String|子机拆除报警|否
tfivepostion|String|发动机|否
tsixpostion|String|低电报警大于2-25%小于|否
tsevenpostion|String|第三字节 第七位   车辆超速|否
fozeropostion|String|第四字节第一位 盗警|否
foonepostion|String|劫警|否
fotwopostion|String|超速报警|否
fothreepostion|String|非法点火报警|否
fofourpostion|String|禁止驶入越界报警|否
fofivepostion|String|GPS天线开路报警|否
fosixpostion|String|GPS天线短路报警|否
fosevenpostion|String|第四字节第七位 禁止驶出越界报警|否
iccid|String|iccid号|否
baseStation1|String||否
baseStation2|String||否
baseStation3|String||否
infolevel|String||否
starnum|String||否
starheight|String||否
pageNum|int|页码|否
pageSize|int|行数|否
params.beginTime|Date|开始时间|否
params.endTime|Date|结束时间|否

**请求示例:**
```
{
	"id":Integer,
	"datatype":Integer,
	"datahead":String,
	"sn":String,
	"ondevicecount":String,
	"ararmstate":String,
	"recordnum":String,
	"deviceid":String,
	"devicetime":Long,
	"devicedate":String,
	"longitude":String,
	"latitude":String,
	"tempone":String,
	"temptwo":String,
	"temponestate":String,
	"temptwostate":String,
	"speedandirection":String,
	"locationstate":String,
	"power":String,
	"productid":String,
	"protocolnum":String,
	"responsetype":String,
	"mnc":String,
	"mcc":String,
	"lacone":String,
	"cione":String,
	"lactwo":String,
	"citwo":String,
	"lacthr":String,
	"cithr":String,
	"oilone":String,
	"oiltwo":String,
	"datalength":String,
	"mileage":String,
	"createtime":Long,
	"del":Integer,
	"speed":String,
	"direction":String,
	"deviceState":String,
	"fzeropostion":String,
	"fonepostion":String,
	"ftwopostion":String,
	"fthreepostion":String,
	"ffourpostion":String,
	"ffivepostion":String,
	"fsixpostion":String,
	"fsevenpostion":String,
	"szeropostion":String,
	"sonepostion":String,
	"stwopostion":String,
	"sthreepostion":String,
	"sfourpostion":String,
	"sfivepostion":String,
	"ssixpostion":String,
	"ssevenpostion":String,
	"tzeropostion":String,
	"tonepostion":String,
	"ttwopostion":String,
	"tthreepostion":String,
	"tfourpostion":String,
	"tfivepostion":String,
	"tsixpostion":String,
	"tsevenpostion":String,
	"fozeropostion":String,
	"foonepostion":String,
	"fotwopostion":String,
	"fothreepostion":String,
	"fofourpostion":String,
	"fofivepostion":String,
	"fosixpostion":String,
	"fosevenpostion":String,
	"iccid":String,
	"baseStation1":String,
	"baseStation2":String,
	"baseStation3":String,
	"infolevel":String,
	"starnum":String,
	"starheight":String,
	"params.beginTime":"2019-01-01 00:00:00",
	"params.endTime":"2029-01-01 00:00:00"
}

```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
rows|array|集合
total|number|记录总数


**返回示例:**
```
{
	"code":0,
	"msg":"success",
	"total":100,
	"rows":{}
}
```

## 车辆数据根据编号查询
**URL:** http://{server}/biz/devdata/get/{id}

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
id|Integer|主键|<font color=red>是</font>

**请求示例:**
```
http://{server}/biz/devdata/get/123
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
data|object|对象


**返回示例:**
```
{
	"code":0,
	"msg":"success",
	"data":{
		"id":Integer,
		"datatype":Integer,
		"datahead":String,
		"sn":String,
		"ondevicecount":String,
		"ararmstate":String,
		"recordnum":String,
		"deviceid":String,
		"devicetime":Long,
		"devicedate":String,
		"longitude":String,
		"latitude":String,
		"tempone":String,
		"temptwo":String,
		"temponestate":String,
		"temptwostate":String,
		"speedandirection":String,
		"locationstate":String,
		"power":String,
		"productid":String,
		"protocolnum":String,
		"responsetype":String,
		"mnc":String,
		"mcc":String,
		"lacone":String,
		"cione":String,
		"lactwo":String,
		"citwo":String,
		"lacthr":String,
		"cithr":String,
		"oilone":String,
		"oiltwo":String,
		"datalength":String,
		"mileage":String,
		"createtime":Long,
		"del":Integer,
		"speed":String,
		"direction":String,
		"deviceState":String,
		"fzeropostion":String,
		"fonepostion":String,
		"ftwopostion":String,
		"fthreepostion":String,
		"ffourpostion":String,
		"ffivepostion":String,
		"fsixpostion":String,
		"fsevenpostion":String,
		"szeropostion":String,
		"sonepostion":String,
		"stwopostion":String,
		"sthreepostion":String,
		"sfourpostion":String,
		"sfivepostion":String,
		"ssixpostion":String,
		"ssevenpostion":String,
		"tzeropostion":String,
		"tonepostion":String,
		"ttwopostion":String,
		"tthreepostion":String,
		"tfourpostion":String,
		"tfivepostion":String,
		"tsixpostion":String,
		"tsevenpostion":String,
		"fozeropostion":String,
		"foonepostion":String,
		"fotwopostion":String,
		"fothreepostion":String,
		"fofourpostion":String,
		"fofivepostion":String,
		"fosixpostion":String,
		"fosevenpostion":String,
		"iccid":String,
		"baseStation1":String,
		"baseStation2":String,
		"baseStation3":String,
		"infolevel":String,
		"starnum":String,
		"starheight":String
	}
}
```


## 车辆数据删除
**URL:** http://{server}/biz/devdata/remove

**Type:** POST

**Content-Type:** application/x-www-form-urlencoded

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
ids|string|id字符串如"1,2,3"|<font color=red>是</font>

**请求示例:**
```
url: 'http://{server}/biz/devdata/remove',
method: 'post',
data: {ids:'1,2,3'}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```