# 设备管理

## 设备地图
**URL:** http://{server}/biz/equipment/maplist

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
managerid|Long|拥有人ID|是
online|Boolean|是否在线(暂不可用)|否

**说明**：会检查查询人和拥有者组织关系，没有关系无法查看

**请求示例:**

```
{
	"managerid":123,
	"online":true
}

```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
rows|array|集合
total|number|记录总数


**返回示例:**
```
{
	"code":0,
	"msg":"success",
	"total":100,
	"rows":{}
}
```
## 设备查询列表
**URL:** http://{server}/biz/equipment/list

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
protocol|Integer|协议类型|否
equtype|Integer|设备类型|否
equnum|String|设备号|否
carnum|String|车牌号|否
simnum|String|sim卡号 可为空|否
overdue|Date|过期时间|否
timeoff|String|不在线时长|否
state|Integer|设备状态|否
equname|String|设备名称|否
pageNum|int|页码|否
pageSize|int|行数|否

**请求示例:**

```
{
	"protocol":Integer,
	"equtype":Integer,
	"equnum":String,
	"carnum":String,
	"simnum":String,
	"overdue":Date,
	"alarm":String,
	"caricon":String,
	"carcolor":String,
	"timezone":String,
	"timeoff":String,
	"state":Integer,
	"equname":String
}

```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
rows|array|集合
total|number|记录总数


**返回示例:**
```
{
	"code":0,
	"msg":"success",
	"total":100,
	"rows":{}
}
```


## 设备根据编号查询
**URL:** http://{server}/biz/equipment/get/{id}

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
id|Integer|主键|<font color=red>是</font>

**请求示例:**
```
http://{server}/biz/equipment/get/123
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
data|object|对象


**返回示例:**
```
{
	"code":0,
	"msg":"success",
	"data":{
		"id":Integer,
		"protocol":Integer,
		"equtype":Integer,
		"equnum":String,
		"carnum":String,
		"simnum":String,
		"overdue":Date,
		"alarm":String,
		"caricon":String,
		"carcolor":String,
		"timezone":String,
		"timeoff":String,
		"speedhigh":String,
		"speedlow":String,
		"fueladd":String,
		"flueldown":String,
		"temphigh":String,
		"templow":String,
		"eleclow":String,
		"overtime":String,
		"overdiver":String,
		"fuelmark":String,
		"pitstop":String,
		"time":Long,
		"del":Integer,
		"managerid":Integer,
		"managername":String,
		"state":Integer,
		"equname":String,
		"czlxr":String,
		"czlxdh":String,
		"czvin":String,
		"czjsz":String,
		"simjhtime":String,
		"simdqtime":String,
		"simcardid":Integer
	}
}
```

## 设备新增
**URL:** http://{server}/biz/equipment/add

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
protocol|Integer|协议类型|<font color=red>是</font>
equtype|Integer|设备类型|<font color=red>是</font>
equnum|String|设备号|<font color=red>是</font>
carnum|String|车牌号|<font color=red>是</font>
simnum|String|sim卡号 可为空|否
overdue|Date|过期时间|否
alarm|String|弹出框，默认每个协议都有勾选，但也可以手动再更改|否
caricon|String|车辆图标 默认图标1|否
carcolor|String|车辆颜色 默认红色|否
timezone|String|时区设置 默认+8 / 东八区|否
timeoff|String|不在线时长|否
speedhigh|String|超速报警|否
speedlow|String|低速报警|否
fueladd|String|加油报警20L|否
flueldown|String|漏油报警20L|否
temphigh|String|高温报警|否
templow|String|低温报警|否
eleclow|String|低电报警|否
overtime|String|超时停车|否
overdiver|String|疲劳驾驶 连续行驶4小时|否
fuelmark|String|油耗标定|否
pitstop|String|中途停车， 保留字段|否
state|Integer|设备状态|否
equname|String|设备名称|<font color=red>是</font>
czlxr|String|车主姓名|<font color=red>是</font>
czlxdh|String|车主电话|<font color=red>是</font>
czvin|String|车主VIN|否
czjsz|String|车主驾驶证|否
simjhtime|String|sim激活时间|否
simdqtime|String|sim失效时间|否
simcardid|Integer|sim卡id|否

**请求示例:**
```
{
	"id":Integer,
	"protocol":Integer,
	"equtype":Integer,
	"equnum":String,
	"carnum":String,
	"simnum":String,
	"overdue":Date,
	"alarm":String,
	"caricon":String,
	"carcolor":String,
	"timezone":String,
	"timeoff":String,
	"speedhigh":String,
	"speedlow":String,
	"fueladd":String,
	"flueldown":String,
	"temphigh":String,
	"templow":String,
	"eleclow":String,
	"overtime":String,
	"overdiver":String,
	"fuelmark":String,
	"pitstop":String,
	"time":Long,
	"del":Integer,
	"managerid":Integer,
	"managername":String,
	"state":Integer,
	"equname":String,
	"czlxr":String,
	"czlxdh":String,
	"czvin":String,
	"czjsz":String,
	"simjhtime":String,
	"simdqtime":String,
	"simcardid":Integer
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```

## 设备更新
**URL:** http://{server}/biz/equipment/edit

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌

**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
id|Integer||<font color=red>是</font>
protocol|Integer|协议类型|否
equtype|Integer|设备类型|否
equnum|String|设备号|否
carnum|String|车牌号|否
simnum|String|sim卡号 可为空|否
overdue|Date|过期时间|否
alarm|String|弹出框，默认每个协议都有勾选，但也可以手动再更改|否
caricon|String|车辆图标 默认图标1|否
carcolor|String|车辆颜色 默认红色|否
timezone|String|时区设置 默认+8 / 东八区|否
timeoff|String|不在线时长|否
speedhigh|String|超速报警|否
speedlow|String|低速报警|否
fueladd|String|加油报警20L|否
flueldown|String|漏油报警20L|否
temphigh|String|高温报警|否
templow|String|低温报警|否
eleclow|String|低电报警|否
overtime|String|超时停车|否
overdiver|String|疲劳驾驶 连续行驶4小时|否
fuelmark|String|油耗标定|否
pitstop|String|中途停车， 保留字段|否
state|Integer|设备状态|否
equname|String|设备名称|否
czlxr|String|车主姓名|否
czlxdh|String|车主电话|否
czvin|String|车主VIN|否
czjsz|String|车主驾驶证|否
simjhtime|String|sim激活时间|否
simdqtime|String|sim失效时间|否
simcardid|Integer|sim卡id|否

**请求示例:**
```
{
	"id":Integer,
	"protocol":Integer,
	"equtype":Integer,
	"equnum":String,
	"carnum":String,
	"simnum":String,
	"overdue":Date,
	"alarm":String,
	"caricon":String,
	"carcolor":String,
	"timezone":String,
	"timeoff":String,
	"speedhigh":String,
	"speedlow":String,
	"fueladd":String,
	"flueldown":String,
	"temphigh":String,
	"templow":String,
	"eleclow":String,
	"overtime":String,
	"overdiver":String,
	"fuelmark":String,
	"pitstop":String,
	"time":Long,
	"del":Integer,
	"managerid":Integer,
	"managername":String,
	"state":Integer,
	"equname":String,
	"czlxr":String,
	"czlxdh":String,
	"czvin":String,
	"czjsz":String,
	"simjhtime":String,
	"simdqtime":String,
	"simcardid":Integer
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```

## 设备删除
**URL:** http://{server}/biz/equipment/remove

**Type:** POST

**Content-Type:** application/x-www-form-urlencoded

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
ids|string|id字符串如"1,2,3"|<font color=red>是</font>

**请求示例:**
```
url: 'http://{server}/biz/equipment/remove',
method: 'post',
data: {ids:'1,2,3'}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```