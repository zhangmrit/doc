# 角色管理

## 角色查询列表
**URL:** http://{server}/sys/role/list

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
roleId|Long|角色ID|否
roleName|String|角色名称|否
roleKey|String|角色权限字符串|否
roleSort|Integer|显示顺序|否
dataScope|String|数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）|否
status|String|角色状态（0正常 1停用）|否
delFlag|String|删除标志（0代表存在 2代表删除）|否
pageNum|int|页码|否
pageSize|int|行数|否
params.beginTime|Date|开始时间|否
params.endTime|Date|结束时间|否

**请求示例:**
```json
{
	"roleId":Long,
	"roleName":String,
	"roleKey":String,
	"roleSort":Integer,
	"dataScope":String,
	"status":String,
	"delFlag":String,
	"params.beginTime":"2019-01-01 00:00:00",
	"params.endTime":"2029-01-01 00:00:00"
}

```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
rows|array|集合
total|number|记录总数


**返回示例:**
```json
{
	"code":0,
	"msg":"success",
	"total":100,
	"rows":[]
}
```

## 角色根据编号查询
**URL:** http://{server}/sys/role/get/{roleId}

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
roleId|Long|主键|<font color=red>是</font>

**请求示例:**
```
http://{server}/sys/role/get/123
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
data|object|对象


**返回示例:**
```json
{
	"code":0,
	"msg":"success",
	"data":{
		"roleId":Long,
		"roleName":String,
		"roleKey":String,
		"roleSort":Integer,
		"dataScope":String,
		"status":String,
		"delFlag":String,
		"createBy":String,
		"createTime":Date,
		"updateBy":String,
		"updateTime":Date,
		"remark":String
	}
}
```

## 角色新增
**URL:** http://{server}/sys/role/add

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
roleName|String|角色名称|<font color=red>是</font>
roleKey|String|角色权限字符串|<font color=red>是</font>
menuIds|array|权限ID（[1,2,3]）|<font color=red>是</font>
roleSort|Integer|显示顺序|<font color=red>是</font>
status|String|角色状态（0正常 1停用）|<font color=red>是</font>
remark|String|备注|否

**请求示例:**

```json
{
	"roleId":Long,
	"roleName":String,
	"roleKey":String,
	"roleSort":Integer,
	"dataScope":String,
	"status":String,
	"delFlag":String,
	"remark":String
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```json
{
	"code":0,
	"msg":"success"
}
```

## 角色更新
**URL:** http://{server}/sys/role/edit

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
roleId|Long|角色ID|<font color=red>是</font>
menuIds|array|权限ID（[1,2,3]）|<font color=red>是</font>
roleName|String|角色名称|否
roleKey|String|角色权限字符串|否
roleSort|Integer|显示顺序|否
status|String|角色状态（0正常 1停用）|否
delFlag|String|删除标志（0代表存在 2代表删除）|否
remark|String|备注|否

**请求示例:**

```json
{
	"roleId":Long,
	"roleName":String,
	"roleKey":String,
	"roleSort":Integer,
	"dataScope":String,
	"status":String,
	"menuIds":[1,2,3],
	"delFlag":String,
	"remark":String
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```json
{
	"code":0,
	"msg":"success"
}
```

## 角色删除
**URL:** http://{server}/sys/role/remove

**Type:** POST

**Content-Type:** application/x-www-form-urlencoded

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
ids|string|id字符串如"1,2,3"|<font color=red>是</font>

**请求示例:**
```
url: 'http://{server}/sys/role/remove',
method: 'post',
data: {ids:'1,2,3'}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```json
{
	"code":0,
	"msg":"success"
}
```

## 数据权限
**URL:** http://{server}/sys/role/rule

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
roleId|Long|角色ID|<font color=red>是</font>
dataScope|string|数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）|<font color=red>是</font>
deptIds|array|部门编号，dataScope='2'时必填，否则报错,其他不要填|否

**请求示例:**
```json

{
	"roleId":Long,
    "dataScope":"2",
	"deptIds":[1,2,3]
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```json
{
	"code":0,
	"msg":"success"
}
```