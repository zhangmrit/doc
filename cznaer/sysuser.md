# 用户管理
## 用户组织列表
**URL:** http://{server}/sys/user/org

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌

**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
pid|Long|上级编号|<font color=red>是</font>

**说明**：会检查用户层级关系，不属于层级关系内无法查询

**请求示例:**

```json
{
	"pid":1
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
rows|array|集合
total|number|记录总数

**返回示例:**

```json
{
	"code":0,
	"msg":"success",
	"total":100,
	"rows":[]
}
```

## 用户查询列表
**URL:** http://{server}/sys/user/list

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌

**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
userId|Long|用户ID|否
deptId|Long|部门ID|否
loginName|String|登录账号|否
userName|String|用户昵称|否
userType|String|用户类型（00系统用户）|否
email|String|用户邮箱|否
phonenumber|String|手机号码|否
sex|String|用户性别（0男 1女 2未知）|否
avatar|String|头像路径|否
password|String|密码|否
salt|String|盐加密|否
status|String|帐号状态（0正常 1停用）|否
delFlag|String|删除标志（0代表存在 2代表删除）|否
loginIp|String|最后登陆IP|否
loginDate|Date|最后登陆时间|否
pid|Long|上级编号|否
pageNum|int|页码|否
pageSize|int|行数|否
params.beginTime|Date|开始时间|否
params.endTime|Date|结束时间|否

**请求示例:**
```json
{
	"userId":Long,
	"deptId":Long,
	"loginName":String,
	"userName":String,
	"userType":String,
	"email":String,
	"phonenumber":String,
	"sex":String,
	"avatar":String,
	"password":String,
	"salt":String,
	"status":String,
	"delFlag":String,
	"loginIp":String,
	"loginDate":Date,
	"params.beginTime":"2019-01-01 00:00:00",
	"params.endTime":"2029-01-01 00:00:00"
}

```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
rows|array|集合
total|number|记录总数

**返回示例:**

```json
{
	"code":0,
	"msg":"success",
	"total":100,
	"rows":[]
}
```

## 用户根据编号查询
**URL:** http://{server}/sys/user/get/{userId}

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
userId|Long|主键|<font color=red>是</font>

**请求示例:**
```
http://{server}/sys/user/get/123
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
data|object|对象


**返回示例:**
```json
{
	"code":0,
	"msg":"success",
	"data":{
		"userId":Long,
		"deptId":Long,
		"loginName":String,
		"userName":String,
		"userType":String,
		"email":String,
		"phonenumber":String,
		"sex":String,
		"avatar":String,
		"password":String,
		"salt":String,
		"status":String,
		"delFlag":String,
		"loginIp":String,
		"loginDate":Date,
		"createBy":String,
		"createTime":Date,
		"updateBy":String,
		"updateTime":Date,
		"remark":String
	}
}
```

## 用户新增
**URL:** http://{server}/sys/user/add

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
deptId|Long|部门ID|否
loginName|String|登录账号|否
userName|String|用户昵称|否
userType|String|用户类型（00系统用户）|否
email|String|用户邮箱|否
phonenumber|String|手机号码|否
sex|String|用户性别（0男 1女 2未知）|否
avatar|String|头像路径|否
password|String|密码|否
status|String|帐号状态（0正常 1停用）|否
remark|String|备注|否

**说明**：添加人默认成为被添加用户的上级

**请求示例:**

```json
{
	"userId":Long,
	"deptId":Long,
	"loginName":String,
	"userName":String,
	"userType":String,
	"email":String,
	"phonenumber":String,
	"sex":String,
	"avatar":String,
	"password":String,
	"salt":String,
	"status":String,
	"delFlag":String,
	"loginIp":String,
	"loginDate":Date,
	"remark":String
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```json
{
	"code":0,
	"msg":"success"
}
```

## 用户更新
**URL:** http://{server}/sys/user/edit

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
userId|Long|用户ID|否
deptId|Long|部门ID|否
loginName|String|登录账号|否
userName|String|用户昵称|否
userType|String|用户类型（00系统用户）|否
email|String|用户邮箱|否
phonenumber|String|手机号码|否
sex|String|用户性别（0男 1女 2未知）|否
avatar|String|头像路径|否
password|String|密码|否
salt|String|盐加密|否
status|String|帐号状态（0正常 1停用）|否
delFlag|String|删除标志（0代表存在 2代表删除）|否
loginIp|String|最后登陆IP|否
loginDate|Date|最后登陆时间|否
remark|String|备注|否

**请求示例:**
```json
{
	"userId":Long,
	"deptId":Long,
	"loginName":String,
	"userName":String,
	"userType":String,
	"email":String,
	"phonenumber":String,
	"sex":String,
	"avatar":String,
	"password":String,
	"salt":String,
	"status":String,
	"delFlag":String,
	"loginIp":String,
	"loginDate":Date,
	"remark":String
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```json
{
	"code":0,
	"msg":"success"
}
```

## 用户删除
**URL:** http://{server}/sys/user/remove

**Type:** POST

**Content-Type:** application/x-www-form-urlencoded

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
ids|string|id字符串如"1,2,3"|<font color=red>是</font>

**请求示例:**
```
url: 'http://{server}/sys/user/remove',
method: 'post',
data: {ids:'1,2,3'}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```json
{
	"code":0,
	"msg":"success"
}
```