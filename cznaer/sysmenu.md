# 菜单权限

## 菜单权限查询列表
**URL:** http://{server}/sys/menu/list

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
menuId|Long|菜单ID|否
menuName|String|菜单名称|否
parentId|Long|父菜单ID|否
orderNum|Integer|显示顺序|否
url|String|请求地址|否
target|String|打开方式（menuItem页签 menuBlank新窗口）|否
menuType|String|菜单类型（M目录 C菜单 F按钮）|否
visible|String|菜单状态（0显示 1隐藏）|否
perms|String|权限标识|否
icon|String|菜单图标|否
pageNum|int|页码|否
pageSize|int|行数|否
params.beginTime|Date|开始时间|否
params.endTime|Date|结束时间|否

**请求示例:**
```
{
	"menuId":Long,
	"menuName":String,
	"parentId":Long,
	"orderNum":Integer,
	"url":String,
	"target":String,
	"menuType":String,
	"visible":String,
	"perms":String,
	"icon":String,
	"params.beginTime":"2019-01-01 00:00:00",
	"params.endTime":"2029-01-01 00:00:00"
}

```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
rows|array|集合
total|number|记录总数


**返回示例:**
```
{
	"code":0,
	"msg":"success",
	"total":100,
	"rows":{}
}
```

## 菜单权限根据编号查询
**URL:** http://{server}/sys/menu/get/{menuId}

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
menuId|Long|主键|是

**请求示例:**
```
http://{server}/sys/menu/get/123
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
data|object|对象


**返回示例:**
```
{
	"code":0,
	"msg":"success",
	"data":{
		"menuId":Long,
		"menuName":String,
		"parentId":Long,
		"orderNum":Integer,
		"url":String,
		"target":String,
		"menuType":String,
		"visible":String,
		"perms":String,
		"icon":String,
		"createBy":String,
		"createTime":Date,
		"updateBy":String,
		"updateTime":Date,
		"remark":String
	}
}
```

## 菜单权限新增
**URL:** http://{server}/sys/menu/add

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
menuId|Long|菜单ID|否
menuName|String|菜单名称|否
parentId|Long|父菜单ID|否
orderNum|Integer|显示顺序|否
url|String|请求地址|否
target|String|打开方式（menuItem页签 menuBlank新窗口）|否
menuType|String|菜单类型（M目录 C菜单 F按钮）|否
visible|String|菜单状态（0显示 1隐藏）|否
perms|String|权限标识|否
icon|String|菜单图标|否
remark|String|备注|否

**请求示例:**
```
{
	"menuId":Long,
	"menuName":String,
	"parentId":Long,
	"orderNum":Integer,
	"url":String,
	"target":String,
	"menuType":String,
	"visible":String,
	"perms":String,
	"icon":String,
	"remark":String
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```

## 菜单权限更新
**URL:** http://{server}/sys/menu/edit

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
menuId|Long|菜单ID|否
menuName|String|菜单名称|否
parentId|Long|父菜单ID|否
orderNum|Integer|显示顺序|否
url|String|请求地址|否
target|String|打开方式（menuItem页签 menuBlank新窗口）|否
menuType|String|菜单类型（M目录 C菜单 F按钮）|否
visible|String|菜单状态（0显示 1隐藏）|否
perms|String|权限标识|否
icon|String|菜单图标|否
remark|String|备注|否

**请求示例:**
```
{
	"menuId":Long,
	"menuName":String,
	"parentId":Long,
	"orderNum":Integer,
	"url":String,
	"target":String,
	"menuType":String,
	"visible":String,
	"perms":String,
	"icon":String,
	"remark":String
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```

## 菜单权限删除
**URL:** http://{server}/sys/menu/remove/{menuId}

**Type:** POST

**Content-Type:** application/x-www-form-urlencoded

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
menuId|Long|菜单编号|是

**请求示例:**
```
url: 'http://{server}/sys/menu/remove/1',
method: 'post'
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```

## 校验菜单名称
**URL:** http://{server}/sys/menu/checkMenuNameUnique

**Type:** POST

**Content-Type:** application/x-www-form-urlencoded

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
menuId|Long|菜单编号|<font color="red">是</font>
menuName|string|菜单名称|<font color="red">是</font>
parentId|Long|上级菜单编号|<font color="red">是</font>

**请求示例:**
```
url: 'http://{server}/sys/menu/checkMenuNameUnique',
method: 'post',
data:{
	menuId:110,
	menuName:'哈哈哈',
	parentId:100
}
```
**响应数据:**返回字符串类型0或1，0表示唯一，1表示不唯一

**返回示例:**

```
0
```

## 加载角色菜单列表树
**URL:** http://{server}/sys/menu/roleMenuTreeData

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
roleId|Long|角色编号|<font color="red">是</font>

**请求示例:**

```
url: 'http://{server}/sys/menu/roleMenuTreeData?roleId=1'
```
**返回示例:**

```
[
    {
        "name": "字典查询<font color=\"#888\">&nbsp;&nbsp;&nbsp;system:dict:list</font>",
        "checked": false,
        "pId": 105,
        "id": 1024,
        "title": "字典查询"
    },
    {
        "name": "参数查询<font color=\"#888\">&nbsp;&nbsp;&nbsp;system:config:list</font>",
        "checked": false,
        "pId": 106,
        "id": 1029,
        "title": "参数查询"
    },...
]
```

## 加载所有菜单列表树
**URL:** http://{server}/sys/menu/menuTreeData

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌

**请求示例:**

```
url: 'http://{server}/sys/menu/menuTreeData'
```
**返回示例:**

```
[
    {
        "name": "字典查询<font color=\"#888\">&nbsp;&nbsp;&nbsp;system:dict:list</font>",
        "checked": false,
        "pId": 105,
        "id": 1024,
        "title": "字典查询"
    },
    {
        "name": "参数查询<font color=\"#888\">&nbsp;&nbsp;&nbsp;system:config:list</font>",
        "checked": false,
        "pId": 106,
        "id": 1029,
        "title": "参数查询"
    },...
]
```