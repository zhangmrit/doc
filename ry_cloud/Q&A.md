# 常见问题

## 验证码无法显示

首先检查网关是否启动，然后检查是否按照 [快速启动](quick-start) 绑定host地址 **90%**都是不看文档，没绑定host导致。如果实在不想绑定host，请使用网关的**真实ip**地址，如`127.0.0.1:9527`

如果还无法显示，请在前端控制面板查看报错信息，后端的大部分原因是没配置`redis`

## 去除验证码拦截

在网关的`xxx.yml`中找到`- ImgCodeFilter`，注释掉即可

## 前端首页加载慢

演示环境默认打包了`tinymce`富文本，大概有4m多(未压缩前)，演示服务器带宽1m🤪，如果你不需要富文本，移除后效果变好，另外你如果是内网环境，那不需要担心，如果你是壕，你当我没说，使用cdn加速整个网站都行

## 第一次查询超时

你可能会碰到第一次登陆的时候返回null，用户名密码错误，第二次就可以正常登陆，这是由于默认数据源没有被初始化，提高系统启动速度。解决办法有两种：

1. 配置超时时间，参看 [gateway超时](/advance?id=gateway超时)

2. 显式声明初始化数据源，配置`initMethod="init"`

   ```java
       @Bean(name="masterDataSource",initMethod="init")
       @ConfigurationProperties("spring.datasource.druid.master")
       public DataSource masterDataSource()
       {
           DruidDataSource dataSource = DruidDataSourceBuilder.create().build();
           return druidProperties.dataSource(dataSource);
       }
   
       @Bean(name="slaveDataSource",initMethod="init")
       @ConfigurationProperties("spring.datasource.druid.slave")
       @ConditionalOnProperty(prefix = "spring.datasource.druid.slave", name = "enabled", havingValue = "true")
       public DataSource slaveDataSource()
       {
           DruidDataSource dataSource = DruidDataSourceBuilder.create().build();
           return druidProperties.dataSource(dataSource);
       }
   ```

## nacos相关

很多时候是各组件版本没有对上，可能或不止于包含`Nacos`，`Spring Cloud Alibabam`,`Sentinel`,`Spring Cloud`,`Spring Boot`,详情请参考[版本说明](https://github.com/alibaba/spring-cloud-alibaba/wiki/版本说明)

1. 配置`nacos.config`必须在`bootstrap.yml`或者`bootstrap.properties`,原因见：[nacos wiki](https://github.com/spring-cloud-incubator/spring-cloud-alibaba/wiki/Nacos-config)

2. `gateway`中配置增加了自动转大写

3. `jar`启动时，`nacos`远程配置内容不能有注释，`ide`中启动没有问题

   有群友提供在启动脚本中加入 `-Dfile.encoding=utf-8`可以解决，未验证，请自行尝试

4. 本实例把`nacos`的配置都放在`nacos.sql`，如果你不需要持久化，也可以从`master`分支`config`中获取，配置内容是一样的

## Activiti 工作流

1. 流程图连线不显示名称

   - 替换doc文件夹中的`activiti-image-generator-5.22.0.jar`，这是官方的一个bug，我下载源码重新编译了

   - 也可以重写画笔工具，参考`com.ruoyi.activiti.cover.CustomProcessDiagramGenerator`

2. linux流程图乱码

   jdk根目录下的 `jre/lib/fonts`目录新建文件夹` fallback  `,上传字体文件到该文件，本项目是**宋体常规**， `simsun.ttc` 

   ```shell
   mkdir fallback
   cd fallback
   mkfontscale
   mkfontdir
   ```

    若找不到命令先进行安装相关工具：`yum install -y fontconfig mkfontscale5` 

3. 打印sql方便调试

   `application.yml(bootstrap.yml)`中修改配置

   ```yaml
   # 日志配置
   logging:
     level:
       org.activiti.engine.impl.persistence.entity: trace
   ```

4. ID策略

   默认是自带的一个发号器有顺序，可以修改成uuid，但是会致使流程图**时序错乱**问题（网关和任务节点有的时候开始时间是一致的）

   - 自定义一个ID策略

     ```java
     @Component
     public class MyIdGenerator implements IdGenerator
     {
         @Override
         public String getNextId()
         {
             String uuid = UUID.randomUUID().toString().replaceAll("-", "");
             return uuid;
         }
     }
     ```

   - 在`ActivitiConfig`配置中加入

     ```java
     // 流程配置
         @Bean
         public ProcessEngineConfigurationConfigurer processEngineConfigurationConfigurer(DataSource dataSource,
                 PlatformTransactionManager transactionManager)
         {
             ProcessEngineConfigurationConfigurer configurer = new ProcessEngineConfigurationConfigurer()
             {
                 @Override
                 public void configure(SpringProcessEngineConfiguration processEngineConfiguration)
                 {
                     // id策略 流程图如果需要追踪，只有默认id策略可以解决连线时序问题
                     processEngineConfiguration.setIdGenerator(idGenerator);
                 }
             };
             return configurer;
         }
     ```

     

