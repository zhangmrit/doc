## 跨域问题

平时被问到最多的问题还是关于跨域的，在`vue`里跨域其实很简单。

我最推荐的也是我工作中在使用的方式就是： `cors` 全称为 Cross Origin Resource Sharing（跨域资源共享）。这种方案对于前端来说没有什么工作量，和正常发送请求写法上没有任何区别，工作量基本都在后端这里。每一次请求，浏览器必须先以 `OPTIONS` 请求方式发送一个预请求（也不是所有请求都会发送 options），通过预检请求从而获知服务器端对跨源请求支持的 `HTTP` 方法。在确认服务器允许该跨源请求的情况下，再以实际的 `HTTP` 请求方法发送那个真正的请求。推荐的原因是：只要第一次配好了，之后不管有多少接口和项目复用就可以了，一劳永逸的解决了跨域问题，而且不管是开发环境还是正式环境都能方便的使用。详细 [MDN 文档](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Access_control_CORS)

但总有后端觉得麻烦不想这么搞，那纯前端也是有解决方案的。

在 `dev` 开发模式下可以下使用 `webpack` 的 `proxy` 使用也是很方便，参照 [文档](https://doc.webpack-china.org/configuration/dev-server/#devserver-proxy) 就会使用了，

```javascript
devServer: {
    // ruoyi-ant 示例代码
    port: 8000,
    proxy: {
      '/api': {
        target: 'http://gateway.com:9527',
        pathRewrite: { '^/api': '' },
        ws: false,
        changeOrigin: true
      }
    }
  }
```

楼主一些个人项目使用的该方法。但这种方法在生产环境是不能使用的。在生产环境中需要使用 `nginx` 进行反向代理。

```nginx
# ruoyi-cloud 示例代码
location ^~ /api/ {
	add_header 'Access-Control-Allow-Origin' *;
	add_header 'Access-Control-Allow-Credentials' 'true';
	add_header 'Access-Control-Allow-Methods' 'GET,POST,OPTIONS';
	proxy_pass http://127.0.0.1:9527/;
	proxy_set_header   X-Real-IP         $remote_addr;
	proxy_set_header   Host              $http_host;
	proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;
}
```

不管是 `proxy` 和 `nginx` 的原理都是一样的，通过搭建一个中转服务器来转发请求规避跨域的问题。

结论：

|   **开发**    | **生产** |
| :-----------: | :------: |
| webpack proxy |  nginx   |

**这里我只推荐这两种方式跨域，其它的跨域方式都还有很多但都不推荐，真心主流的也就这两种方式。**

## 使用字典

`ruoyi-ant`首创字典方法，也是在找不到案例的情况下自己摸索的，符合后台程序使用特点。

字典两种用法，各有优缺点：

1. Map 需要数组的时候构造数组不方便

   ```javascript
   operTypeMap = await getDictMap('sys_oper_type')
   this.operTypeMap = operTypeMap
   this.operTypeMap.forEach((value, key, mymap) => {
   	this.businessTypes.push({ code: key, label: value })
   })
   ```

2. 数组 在modal调用没有map方便

   ```javascript
   this.businessTypes = await getDictArray('sys_oper_type')
   this.businessTypes.map(d => {
   	operTypeMap[d.dictValue] = { text: d.dictLabel }
   })
   this.operTypeMap = operTypeMap
   ```

## gateway超时

1. 如果`hystrix.command.default.execution.timeout.enabled`为`true`,则会有两个执行方法超时的配置,一个就是ribbon的`ReadTimeout`,一个就是熔断器hystrix的`timeoutInMilliseconds`,此时谁的值小谁生效
2. 如果`hystrix.command.default.execution.timeout.enabled`为`false`,则熔断器不进行超时熔断,而是根据ribbon的`ReadTimeout`抛出的异常而熔断,也就是取决于ribbon
3. ribbon的`ConnectTimeout`,配置的是请求服务的超时时间,除非服务找不到,或者网络原因,这个时间才会生效
4. ribbon还有`MaxAutoRetries`对当前实例的重试次数,`MaxAutoRetriesNextServer`对切换实例的重试次数,如果ribbon的`ReadTimeout`超时,或者`ConnectTimeout`连接超时,会进行重试操作
5. 由于ribbon的重试机制,通常熔断的超时时间需要配置的比`ReadTimeout`长,`ReadTimeout`比`ConnectTimeout`长,否则还未重试,就熔断了
6. 为了确保重试机制的正常运作,理论上（以实际情况为准）建议hystrix的超时时间为:(`1 + MaxAutoRetries + MaxAutoRetriesNextServer) * ReadTimeout`

有的时候超时时间太短，有必要可以按下列设置，`ide`可能报警告，无视，亲测有效

```yaml
ribbon:
  OkToRetryOnAllOperations: false #对所有操作请求都进行重试,默认false
  ReadTimeout: 5000   #负载均衡超时时间，默认值5000
  ConnectTimeout: 2000 #ribbon请求连接的超时时间，默认值2000
  MaxAutoRetries: 0     #对当前实例的重试次数，默认0
  MaxAutoRetriesNextServer: 1 #对切换实例的重试次数，默认1
hystrix:
  command:
    default:  #default全局有效，service id指定应用有效
      execution:
        timeout:
          enabled: true
        isolation:
          thread:
            timeoutInMilliseconds: 10000 #断路器超时时间，默认1000ms
```

## 导出文件

在若依中是生成临时文件存到本地磁盘然后再下载，但是在微服务中不可取，各自服务间无法相互调用，方便演示，都在`system`模块下，如果在实际生产中使用，需要自行改造，有以下几个方案可供选择：

1. 不生成临时文件，直接以流形式输出并下载
2. 生成文件后上传到`oss`或文件系统，返回链接并下载

## swagger文档

代码里只是加了一个演示功能，正式使用请**注意**

1. 切换`SwaggerProvider`注释

   ```java
   @Override
       public List<SwaggerResource> get()
       {
           List<SwaggerResource> resources = new ArrayList<>();
           List<String> routes = new ArrayList<>();
           // 取出gateway的route
           routeLocator.getRoutes().subscribe(route -> routes.add(route.getId()));
           // 结合配置的route-路径(Path)，和route过滤，只获取有效的route节点
           gatewayProperties.getRoutes().stream()
               .filter(routeDefinition -> routes.contains(routeDefinition.getId()))
       	...
           return resources;
       }
   ```

   

2. 在接入`gateway`的微服务中引入`common-swagger`

   使用方法请参考`system`模块

   访问地址：`gateway地址`:`端口号/doc.html`

   ```
   127.0.0.1:9527/doc.html
   ```

## 国际化

用`i18n`实现了一个小例子，可以在工作台页面查看效果，具体配置在`/src/locales`文件下

## 通用mapper

关于通用`mapper`，本来不想介绍的，太基础了，不过鉴于规范，还是啰嗦一下吧

1. 添加依赖

   ```xml
   <dependency>
     <groupId>tk.mybatis</groupId>
     <artifactId>mapper-spring-boot-starter</artifactId>
     <version>版本号</version>
   </dependency>
   ```

   > 最新版本号如上所示，你也可以从下面地址查看：
   >
   > http://mvnrepository.com/artifact/tk.mybatis/mapper-spring-boot-starter
   >
   > **注意：引入该 starter 时，和 MyBatis 官方的 starter 没有冲突，但是官方的自动配置不会生效！**

2. 配置

    如果你需要对通用 Mapper 进行配置，你可以在 Spring Boot 的配置文件中配置 `mapper.` 前缀的配置。 

    例如在 yml 格式中配置： 

   ```yaml
   mapper:
     mappers:
       - tk.mybatis.mapper.common.Mapper
       - tk.mybatis.mapper.common.Mapper2
     notEmpty: true
   ```

    在 properties 配置中： 

   ```properties
   mapper.mappers=tk.mybatis.mapper.common.Mapper,tk.mybatis.mapper.common.Mapper2
   mapper.notEmpty=true
   ```

    由于 Spring Boot 支持 Relax 方式的参数，因此你在配置 `notEmpty` 时更多的是用 `not-empty`，也只有在 Spring Boot 中使用的时候参数名不必和配置中的完全一致。 

3. ##### `@MapperScan` 注解配置

    你可以给带有 `@Configuration` 的类配置该注解，或者直接配置到 Spring Boot 的启动类上，如下： 

   ```java
   @tk.mybatis.spring.annotation.MapperScan(basePackages = "扫描包")
   @SpringBootApplication
   public class SampleMapperApplication
   ```

    **注意：这里使用的 `tk.mybatis.spring.annotation.MapperScan` !** 

4. 模型中添加注解

    最简单的情况下，只需要一个 `@Id` 标记字段为主键即可。数据库中的字段名和实体类的字段名是完全相同的，这中情况下实体和表可以直接映射。 

    **提醒：如果实体类中没有一个标记 `@Id` 的字段，当你使用带有 `ByPrimaryKey` 的方法时，所有的字段会作为联合主键来使用，也就会出现类似 `where id = ? and countryname = ? and countrycode = ?`的情况。** 

5. mapper扩展

    **如果想要增加自己写的方法，可以直接在 `Mapper` 中增加。** 

   -  **使用纯接口注解方式时** 

     ```java
     import org.apache.ibatis.annotations.Select;
     import tk.mybatis.mapper.common.Mapper;
     
     public interface CountryMapper extends Mapper<Country> {
         @Select("select * from country where countryname = #{countryname}")
         Country selectByCountryName(String countryname);
     }
     ```

   -  **如果使用 XML 方式，需要提供接口对应的 XML 文件** 

      例如提供了 `CountryMapper.xml` 文件，内容如下： 

     ```xml
     <!DOCTYPE mapper
             PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
             "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
     <mapper namespace="tk.mybatis.sample.mapper.CountryMapper">
         <select id="selectByCountryName" resultType="tk.mybatis.model.Country">
             select * from country where countryname = #{countryname}
         </select>
     </mapper>
     ```

      在接口中添加对应的方法： 

     ```java
     import tk.mybatis.mapper.common.Mapper;
     
     public interface CountryMapper extends Mapper<Country> {
         Country selectByCountryName(String countryname);
     }
     ```

## mybatis-plus

`springcloud`和`springboot`一样，`boot`怎么集成就怎么集成，和`cloud`无关，具体看[mp文档](https://mp.baomidou.com/guide/quick-start.html)，搞过的可以开源一下，平时就别问了，容易被笑话，有开源地址我会在这里列出

## 富文本

以下内容摘自 [vue-element-admin/富文本](https://panjiachen.gitee.io/vue-element-admin-site/zh/feature/component/rich-editor.html)

富文本是管理后台一个核心的功能，但同时又是一个有很多坑的地方。在选择富文本的过程中我也走了不少的弯路，市面上常见的富文本都基本用过了，最终权衡了一下选择了[Tinymce](https://github.com/tinymce/tinymce)。

这里在简述一下**panjiachen**推荐使用 tinymce 的原因：tinymce 是一家老牌做富文本的公司(这里也推荐 ckeditor，也是一家一直做富文本的公司，新版本很不错)，它的产品经受了市场的认可，不管是文档还是配置的自由度都很好。在使用富文本的时候有一点也很关键就是复制格式化，之前在用一款韩国人做的富文本 summernote 被它的格式化坑的死去活来，但 tinymce 的去格式化相当的好，它还有一些增值服务(付费插件)，最好用的就是`powerpaste`，非常的强大，支持从 word 里面复制各种东西，而且还帮你顺带格式化了。富文本还有一点也很关键，就是拓展性。楼主用 tinymce 写了好几个插件，学习成本和容易度都不错，很方便拓展。最后一点就是文档很完善，基本你想得到的配置项，它都有。tinymce 也支持按需加载，你可以通过它官方的 build 页定制自己需要的 plugins。

#### 常见富文本

再来分析一下市面上其它的一些富文本：

- **[summernote](https://github.com/summernote/summernote)** 先来说一个我绝对不推荐的富文本。这是一个韩国人开源的富文本(当然不推荐的理由不是因为这个)，它对很多富文本业界公认的默认行为理解是反其道而行的，而且只为用了一个 dialog 的功能，引入了 bootstrap，一堆人抗议就是不改。格式化也是差劲。。反正不要用！不要用！不要用！
- **[ckeditor](https://github.com/galetahub/ckeditor)** ckeditor 也是一家老牌做富文本的公司，楼主旧版后台用的就是这个，今年也出了 5.0 版本，ui 也变美观了不少，相当的不错，而且它号称是插件最丰富的富文本了。推荐大家也可以试用一下。
- **[quill](https://github.com/quilljs/quill)** 也是一个非常火的富文本，长相很不错。基于它写插件也很简单，api 设计也很简单。楼主不选择它的原因是它对图片的各种操作不友善，而且很难改。如果对图片没什么操作的用户，推荐使用。
- **[medium-\*editor\*](https://github.com/yabwe/medium-editor)** 大名鼎鼎的 medium 的富文本(非官方出品)，但完成度还是很不错，拓展性也不错。不过我觉得大部分用户还是会不习惯 medium 这种写作方式的。
- **[squire](https://github.com/neilj/Squire)** 一个比较轻量的富文本，压缩完才 11.5kb，相对于其它的富文本来说是非常的小了，推荐功能不复杂的建议使用。
- **[wangEditor](https://github.com/wangfupeng1988/wangEditor)** 一个国人写的富文本，用过感觉还是不错的。不过毕竟是个人的，不像专门公司做富文本的，配置型和丰富性不足。前端几大禁忌就有富文本 [为什么都说富文本编辑器是天坑?](https://www.zhihu.com/question/38699645)，不过个人能做成这样子很不容易了。
- **[百度 UEditor](http://ueditor.baidu.com/website/index.html)** 没有深入使用过，只在一个 angular1X 的项目简单用过，不过说着的 ui 真的不好看，不符合当今审美了，官方也已经很久没更新过了。
- **[slate](https://github.com/ianstormtaylor/slate)** 是一个 完全 可定制的富文本编辑框架。通过 Slate，你可以构建出类似 Medium、Dropbox Paper 或者 Canvas 这样使用直观、富交互、体验业已成为 Web 应用标杆的编辑器。同时，你也无需担心在代码实现上陷入复杂度的泥潭之中。看起来很酷，以后有机会我会在项目中实践试用一下。

**panjiachen**列举了很多富文本但并没有列举任何 vue 相关的富文本，主要是因为富文本真的比想象中复杂，在前面的文章里也说过了，其实用 vue 封装组件很方便的，没必要去用人家封装的东西。什么 vue-quill vue-editor 这种都只是简单包了一层，没什么难度的。还不如自己来封装，灵活性可控性更强一点。还有一点基于 vue 真没什么好的富文本，不像 react 有 facebook 出的 [draft-js](https://github.com/facebook/draft-js)，ory 出的 [editor](https://github.com/ory/editor)，这种大厂出的产品。

当然你也可以选择一些付费的富文本编辑器，**panjiachen**公司里面有一个项目就使用了 [froala-editor](https://www.froala.com/wysiwyg-editor) 这款编辑器。不管是美观和易用性都是不错的，公司买的是专业版，一年也就 `$349` ，价格也是很合理的，但其实省去的程序员开发成本可能远不止这个价钱。

------

tinymce4分支是使用panjiachen集成的4.9.5版本

tinymce5分支(已经同步到master)是用tinymce-vue结合cdn

## 分布式事务

近期暂没有集成的计划，如果需要可以自己尝试一下，完成后亦可开源或者pr

推荐 [txlcn](http://www.txlcn.org/zh-cn/)、mq(基于可靠消息的最终一致性方案详解)、阿里分布式事务[seata](https://seata.io/zh-cn/)

[分布式事务中常见的三种解决方案](https://www.cnblogs.com/bluemiaomiao/p/11216380.html)

## 多数据源

`RuoYi`是采用注解式使用多数据源，在实际生产中或者已有项目很少会这么做（这么多接口，丧心病狂啊 :joy:）

所以`cloud`或`zhangmrit/Ruoyi`对`DataSourceAspect`进行了改造，其他代码一行未改，简单的支持根据方法名区分读写分离，也可以扩展一主多从，从库轮询读取之类（如果你连`aop`都不知道，应该自我反思:broken_heart:）

不同业务数据库或者不同类型数据库，建议分包处理如`com.ruoyi.mapper1`,`com.ruoyi.mapper2`，微服务出现后，这种情况理应排除，不然你要微服务干嘛

关于多数据源的文章很多，这里不再赘述，只传达一点想法

## 监控

使用`springadmin`完成,目前只是最简单的用法

客户端必须添加依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

还需设置监控端点

```yaml
# 暴露监控端点
management:
  endpoints:
    web:
      exposure:
        include: '*' 
```

***坑点比较多,所以放到这里来讲***

1. 查看日志功能

   ```yaml
   #该功能需和日志配置结合
   logging:
     file: /var/log/sample-boot-application.log
     #file： fileDir
     #还可以加path 请自行查阅
     pattern:
       file: clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} %clr(%5p) %clr(${PID}){magenta} %clr(---){faint} %clr([%15.15t]){faint} %clr(%-40.40logger{39}){cyan} %clr(:){faint} %m%n%wEx
   #暴露监控端点
   management:
     endpoints:
       web:
         exposure:
           include: '*'
     endpoint:
       logfile:
         enabled:  
   ```

2. 修改日志级别

   需要添加一些包和配置

3. security安全相关

   ```yaml
   #添加对应的配置
   spring:
     security:
       user:
         name: "admin"
         password: "admin"
   
   eureka:
     instance:
         leaseRenewalIntervalInSeconds: 10
         health-check-url-path: /actuator/health
         prefer-ip-address: true
         metadata-map:
               user.name: ${spring.security.user.name}
               user.password: ${spring.security.user.password}
   ```

   

4. 邮件通知

   我可能暂时不需要这个功能就没加

## 限流插件

#### Hystrix

在分布式环境中，许多服务依赖项中的一些必然会失败。Hystrix是一个库，通过添加延迟容忍和容错逻辑，帮助你控制这些分布式服务之间的交互。Hystrix通过隔离服务之间的访问点、停止级联失败和提供回退选项来实现这一点，所有这些都可以提高系统的整体弹性。

默认集成`hystrix`，这里不再赘述。

#### Sentinel 

阿里推出的插件，可以结合nacos或者单独使用

##### 控制台

从 [release 页面](https://github.com/alibaba/Sentinel/releases) 下载最新版本的控制台 jar 包，要版本对应，参考[版本说明](https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E)

自己写一个启动批处理`run.bat`

```bash
title=sentinel-dashboard
java -Dserver.port=8880 -Dcsp.sentinel.dashboard.server=localhost:8880 -Dproject.name=sentinel-dashboard -jar sentinel-dashboard-1.x.x.jar
```



##### Spring Cloud Gateway 支持

本项目只介绍集成`gateway`，首先添加依赖

```xml
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
</dependency>

<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-alibaba-sentinel-gateway</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-gateway</artifactId>
</dependency>
```

`bootstrap.yml`中添加

```yaml
spring: 
  cloud:
    sentinel:
      datasource:
       ds1:
        file:
         file: "classpath: api.json"
         rule-type: gw-api-group
       ds2:
        file: 
         file: "classpath: gateway.json"
         rule-type: gw-flow
      transport:
        dashboard: 127.0.0.1:8880
      filter:
        enabled: true
      scg.fallback:
          mode: response
          response-body: "限流啦"
      scg:
        order: -1000
```

`resource`中添加`api.json`和`gateway.json`

```json
[
  {
    "apiName": "system_api",
    "predicateItems": [
      {
        "pattern": "/system/**",
        "matchStrategy": 1
      }
    ]
  }
]
```

```json
[
  {
    "resource": "system_api",
    "count": 1
  }
]
```

移除`hystrix`依赖,`bootstrap.yml`移除相关配置，删除配置类`RateLimiterConfiguration`

```yaml
#            hystrix和sentinel功能冲突,删除下列配置
#            - name: RequestRateLimiter
#              args:
#                key-resolver: '#{@remoteAddrKeyResolver}'
#                redis-rate-limiter.replenishRate: 10
#                redis-rate-limiter.burstCapacity: 20
#              # 降级配置
#            - name: Hystrix
#              args:
#                name: fallbackcmd
#                fallbackUri: 'forward:/fallback'
```

访问system模块接口可以查看限流效果

如果不习惯`json`，可以手写配置类

```java
package com.ruoyi.gateway.config;

import static org.springframework.web.reactive.function.BodyInserters.fromObject;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebExchange;

import com.alibaba.csp.sentinel.adapter.gateway.common.SentinelGatewayConstants;
import com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiDefinition;
import com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiPathPredicateItem;
import com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiPredicateItem;
import com.alibaba.csp.sentinel.adapter.gateway.common.api.GatewayApiDefinitionManager;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayRuleManager;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.BlockRequestHandler;

import reactor.core.publisher.Mono;

@Configuration
public class GatewayConfiguration
{
    //锁定,等效scg.fallback配置
    @Bean
    public BlockRequestHandler blockRequestHandler()
    {
        return new BlockRequestHandler()
        {
            @Override
            public Mono<ServerResponse> handleRequest(ServerWebExchange exchange, Throwable t)
            {
                System.out.println("进入限流");
                return ServerResponse.status(444).contentType(MediaType.APPLICATION_JSON_UTF8)
                        .body(fromObject("SCS Sentinel block"));
            }
        };
    }

    //加载规则，等效于ds1,ds2
    @PostConstruct
    public void doInit()
    {
        initCustomizedApis();
        initGatewayRules();
    }

    private void initCustomizedApis()
    {
        Set<ApiDefinition> definitions = new HashSet<>();
        ApiDefinition api1 = new ApiDefinition("system_api").setPredicateItems(new HashSet<ApiPredicateItem>()
        {
            //
            private static final long serialVersionUID = -2208473312881028001L;
            {
                add(new ApiPathPredicateItem().setPattern("/system/**")
                        .setMatchStrategy(SentinelGatewayConstants.URL_MATCH_STRATEGY_PREFIX));
            }
        });
        definitions.add(api1);
        GatewayApiDefinitionManager.loadApiDefinitions(definitions);
    }

    private void initGatewayRules()
    {
        Set<GatewayFlowRule> rules = new HashSet<>();
        rules.add(new GatewayFlowRule("system_api").setCount(1).setIntervalSec(1));
        GatewayRuleManager.loadRules(rules);
    }
}
```

查看配置规则可以添加

```java
package com.ruoyi.gateway.config;

import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiDefinition;
import com.alibaba.csp.sentinel.adapter.gateway.common.api.GatewayApiDefinitionManager;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayRuleManager;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;

import reactor.core.publisher.Mono;

@RestController
public class RulesWebFluxController
{
    @GetMapping("/api")
    public Mono<Set<ApiDefinition>> apiRules()
    {
        return Mono.just(GatewayApiDefinitionManager.getApiDefinitions());
    }

    @GetMapping("/gateway")
    public Mono<Set<GatewayFlowRule>> apiGateway()
    {
        return Mono.just(GatewayRuleManager.getRules());
    }

    @GetMapping("/flow")
    public Mono<List<FlowRule>> apiFlow()
    {
        return Mono.just(FlowRuleManager.getRules());
    }
}
```

## 修改config配置中心端口号

内置端口号是`8888`，可能有人需要修改此端口号，`server`端修改以后，`client`端仍然连的`8888`无法连接，其实是可以修改的，理解`config`的工作原理，是在读取远端配置后配置了`application.yml`,所以配置在`application.yml` 是不会起作用的,只要新建一个`bootstrap.yml`或者 `bootstrap.properties` 即可

```yaml
spring:
  cloud:
    config:
      uri: http://localhost:8688
```

**tips**:

bootstrap.yml（bootstrap.properties）用来程序引导时执行，应用于更加早期配置信息读取，如可以使用来配置application.yml中使用到参数等

application.yml（application.properties) 应用程序特有配置信息，可以用来配置后续各个模块中需使用的公共参数等。

bootstrap.yml 先于 application.yml 加载

## feign复杂参数和多文件

默认的feign参数有下列规则

1. `SpringCloud`中微服务之间的调用，传递参数时需要加相应的注解。用到的主要是三个注解`@RequestBody，@RequestParam()，@PathVariable()`
2. get和post请求中对于传递单个引用类型的参数，比如String，Integer....用`@RequestParam()`，括号中一定要有值(参数的别名)。调用方需要加注解，被调用方不需要加。当然加上也不会出错。被调用方的参数名和调用方的别名保持一致即可。
3. post请求中对于javaBean，map，list类型的参数的传递，用`@RequestBody`，调用方不需要加注解，被调用方加注解即可。
   注：get请求中使用`@RequestBody`会出错，同时也不能传递javaBean，map，list类型的参数
4. 参数位于路径中用`@PathVariable()`，调用方需要指定别名。别调用方加注解即可。
5. 引用类型的参数传递时可以自动转化。比如调用方传递String类型的“11”，被调用方可以直接用Integer变量接收
6. 返回值为javaBean，可以用String接收JSON字符串，然后自行转化。也可以用javaBean或者Map接收。调用方的属性名和类型要和被调用方的保持一致

默认只有单文件和普通参数传递，对象则必须用`@requestBody`,如果需要实现复杂参数或者多文件，需要做点小手术

- 只需要支持复杂参数，不想要多文件上传

  spring cloud在2.1.x版本中提供了`@SpringQueryMap`注解，可以传递对象参数，框架自动解析 

  ```java
  @FeignClient(name = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteTestFallbackFactory.class)
  public interface RemoteTestService
  {
      @RequestMapping(path = "test")
      public R test(@SpringQueryMap Map<String, Object> params);
  }
  ```

- 复杂参数无法满足，或需要多文件支持，或者其他自定义需求

  1. 重写`SpringFormEncoder`,可以对比源码，其实就是增加了对多文件和Map的解析

     ```java
      public class MyEncoder extends SpringFormEncoder
      {
          public MyEncoder()
          {
              this(new Encoder.Default());
          }
          public MyEncoder(Encoder delegate)
          {
              super(delegate);
              val processor = (MultipartFormContentProcessor) getContentProcessor(MULTIPART);
              processor.addFirstWriter(new SpringSingleMultipartFileWriter());
              processor.addFirstWriter(new SpringManyMultipartFilesWriter());
          }
          @SuppressWarnings("unchecked")
          @Override
          public void encode(Object object, Type bodyType, RequestTemplate template) throws EncodeException
          {
              if (((ParameterizedTypeImpl) bodyType).getRawType().equals(Map.class))
              {
                  val data = (Map<String, Object>) object;
                  Set<String> nullSet = new HashSet<>();
                  for (Map.Entry<String, Object> entry : data.entrySet())
                  {
                      if (entry.getValue() == null)
                      {
                          nullSet.add(entry.getKey());
                      }
                  }
                  for (String s : nullSet)
                  {
                      data.remove(s);
                  }
                  super.encode(data, MAP_STRING_WILDCARD, template);
                  return;
              }
              else if (bodyType.equals(MultipartFile.class))
              {
                  val file = (MultipartFile) object;
                  val data = singletonMap(file.getName(), object);
                  super.encode(data, MAP_STRING_WILDCARD, template);
                  return;
              }
              else if (bodyType.equals(MultipartFile[].class))
              {
                  val file = (MultipartFile[]) object;
                  if (file != null)
                  {
                      val data = singletonMap(file.length == 0 ? "" : file[0].getName(), object);
                      super.encode(data, MAP_STRING_WILDCARD, template);
                      return;
                  }
              }
              super.encode(object, bodyType, template);
          }
      }
     ```
  
  2. 新建配置`FormSupportConfig`
  
     ```java
     @Component
     public class FormSupportConfig
     {
         @Autowired
         private ObjectFactory<HttpMessageConverters> messageConverters;
         @Bean
         public Encoder feignFormEncoder()
         {
             // return new SpringFormEncoder(new SpringEncoder(messageConverters));
             return new MyEncoder(new SpringEncoder(messageConverters));
         }
         @Bean
         public Logger.Level logger()
         {
             return Logger.Level.FULL;
         }
     }
     ```
  
  3. 在FeignClient调用时增加配置
  
     ```java
     @FeignClient(name = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteTestFallbackFactory.class,configuration=FormSupportConfig.class)
     public interface RemoteTestService
     {
         @RequestMapping(path = "test",consumes=MediaType.APPLICATION_FORM_URLENCODED_VALUE,,produces=MediaType.ALL_VALUE)
         public R test(@RequestParam Map<String, Object> params);
     }
     ```

## ESLint

不管是多人合作还是个人项目，代码规范都是很重要的。这样做不仅可以很大程度地避免基本语法错误，也保证了代码的可读性。

#### 配置项

所有的配置文件都在 [.eslintrc.js](https://gitee.com/zhangmrit/ruoyi-ant/blob/master/.eslintrc.js) 中。 本项目基本规范是依托于 vue 官方的 eslint 规则做了少许的修改。大家可以按照自己的需求进行定制化配置。

比如：我个人或者项目组习惯于使用4个空格(默认)，但你可能觉得2个空格更顺眼，你可以做如下修改。 进入项目 `.eslintrc.js` 中，找到 `indent`，然后修改为 `"indent": ["error", 2]` 即可。 还有各种各样的配置信息，详情见 [ESLint 文档](https://eslint.org/docs/rules/)。

默认情况下使用了最严格的`plugin:vue/recommended`来校验代码，若你觉得太严格可自行修改。

```js
module.exports = {
  extends: ['plugin:vue/recommended', 'eslint:recommended']
  //你可以修改为  extends: ['plugin:vue/essential', 'eslint:recommended']
}
```

#### 取消 ESLint 校验

如果你不想使用 ESLint 校验（不推荐取消），只要找到 [vue.config.js](https://gitee.com/zhangmrit/ruoyi-ant/blob/master/vue.config.js) 文件。 进行如下设置 `lintOnSave: false` 即可。

```js
module.exports = {
    lintOnSave: false
}
```



#### vscode 配置 ESLint

工欲善其事，必先利其器，安装ESLint插件事半功倍

1. 安装eslint插件

2. 安装并配置完成 ESLint 后，我们继续回到 VSCode 进行扩展设置，依次点击 文件 > 首选项 > 设置 打开 VSCode 配置文件,添加如下配置(下面的配置是新版本和以前有点不太一样，自己看仔细)

   ```json
   "editor.codeActionsOnSave": {
       "source.fixAll.eslint": true,
     },
     "eslint.validate": [
       "typescript",
       "typescriptreact",
       "html",
       "vue"
     ],
     "eslint.format.enable": true,
     "editor.formatOnSave": true,
     "eslint.codeAction.showDocumentation": {}
   ```

   

这样每次保存的时候就可以根据根目录下.eslintrc.js 你配置的 eslint 规则来检查和做一些简单的 fix。每个人和团队都有自己的代码规范，统一就好了，去打造一份属于自己的 eslint 规则上传到 npm 吧，如饿了么团队的 [config](https://www.npmjs.com/package/eslint-config-elemefe)，vue 的 [config](https://github.com/vuejs/eslint-config-vue)。

[vscode 插件和配置推荐](https://github.com/varHarrie/Dawn-Blossoms/issues/10)

#### 更多配置

由于本项目是基于`vue-cli`进行构建，所以更多配置可参考官方[文档](https://cli.vuejs.org/zh/config/#lintonsave)

#### 自动修复

```sh
npm run lint -- --fix
```

运行如上命令，eslint 会自动修复一些简单的错误。

## 不想使用CDN

```js
const assetsCDN = {
//1.清空这里的配置
  css: [],
  js: []
}
// webpack build externals
const prodExternals = {
//2.这里也清空
}

// vue.config.js
const vueConfig = {
  configureWebpack: {
    externals: prodExternals,
    plugins: [
      // Ignore all locale files of moment.js
      //3.删除关于moment的2行配置
      new webpack.DefinePlugin({
        APP_VERSION: `"${require('./package.json').version}"`,
        GIT_HASH: JSON.stringify(GitRevision.version()),
        BUILD_DATE: buildDate
      })
    ]
  },
```

按照上述步骤删除`vue.config.js`中的配置内容，即可默认加载本地，可能会报一些小错误，到时候欢迎`issue`