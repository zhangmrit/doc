# 快速启动

## 环境准备

- JDK >= 1.8(推荐1.8版本)
- MySQL >= 5.5.0 (推荐5.7版本)
- Maven >= 3.0
- lombok 插件
- Redis >=3.2

`mysql5.6`以下存在`datetime(0)`无法导入的问题，可能需要自行替换`(0)->''`

## 服务端启动

### master分支

1. 克隆代码并导入IDE（推荐`idea`或`myeclips`）

   ```bash
   git clone https://gitee.com/zhangmrit/ruoyi-cloud.git
   ```

2. 创建数据库`ry_cloud`，并导入`sql\ry_cloud.sql`,修改`ruoyi-config/src/main/resources/config`中`mysql`和`redis`配置

3. 绑定本地host

   ```
   127.0.0.1 eureka7001.com
   127.0.0.1 gateway.com
   ```

   如果要使用eureka集群，请继续绑定`eureka7002.com`,`eureka7003.com`并修改各项目中的注释部分

4. 依次启动

   ```
   eureka
   config
   gateway
   system
   auth
   gen 代码生成 可选
   dfs 文件上传 可选
   ```

### nacos分支

1. 克隆代码并导入IDE（推荐`idea`或`myeclips`）

   ```bash
   git clone -b nacos https://gitee.com/zhangmrit/ruoyi-cloud.git
   ```

2. 创建数据库`ry_cloud`，并导入`sql\ry_cloud.sql`

3. 搭建`nacos serve`（版本必须>=`1.1.0`），导入`sql/nacos.sql`并[配置持久化](https://nacos.io/zh-cn/docs/deployment.html)启动，如需帮助，请参考[nacos文档](https://nacos.io/zh-cn/docs/quick-start-spring-cloud.html)，一定要在`nacos控制台`中看到导入的配置，然后修改配置文件的`mysql`和`redis`参数

4. 依次启动

   ```
   gateway
   system
   auth
   gen 代码生成 可选
   dfs 文件上传 可选
   ```

*注意：监控模块由于nacos本身提供了，此处暂不可用*

## 前端启动

1. 拉取项目代码(强烈推荐`vscode`开发)

   ```bash
   git clone https://gitee.com/zhangmrit/ruoyi-ant.git
   cd ruoyi-ant
   ```

2. 切换`vue.config.js`中`devServer:proxy:http://gateway.com:9527`，也可以换成你自己的网关

3. 安装依赖（需安装`yarn`,***不太建议使用cnpm或者npm代替yarn,不保证完全可用***）

   ```bash
   yarn install
   ```

4. 开发模式运行

   ```bash
   yarn run serve
   ```

5. 编译项目

   ```bash
   yarn run build
   ```

6. Lints and fixes files

   ```bash
   yarn run lint
   ```
   



## 服务器部署

### 后端项目

建议使用`docker`或者`k8s`，也可以脚本运行，演示环境为了方便（偷懒），直接脚本运行

须先启动`eureka`和`config`（或者`nacos`），再启动其他微服务

### 前端项目

推荐使用`nginx`，这里分享我的相关配置

```nginx
server {
    listen       80;
    server_name  ruoyi.ant.zmrit.com;
	root         /usr/share/nginx/html/dist;

    # gzip config
    gzip on;
    gzip_min_length 1k;
    gzip_comp_level 9;
    gzip_types text/plain application/javascript application/x-javascript text/css application/xml text/javascript application/x-httpd-php image/jpeg image/gif image/png;
    gzip_vary on;
    gzip_disable "MSIE [1-6]\.";
		
    index  index.html index.htm;
		
    location / {
    # 用于配合 browserHistory 使用
        try_files $uri $uri/ /index.html;
        proxy_set_header   X-Real-IP         $remote_addr;
        proxy_set_header   Host              $http_host;
        proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;
        # 如果有资源，建议使用 https + http2，配合按需加载可以获得更好的体验 
        # rewrite ^/(.*)$ https://myzuul.com:9527/$1 permanent;
    }
    
    location ^~ /api/ {
        add_header 'Access-Control-Allow-Origin' *;
        add_header 'Access-Control-Allow-Credentials' 'true';
        add_header 'Access-Control-Allow-Methods' 'GET,POST,OPTIONS';
        # 网关gateway 内网地址
        proxy_pass http://127.0.0.1:9527/;
        proxy_set_header   X-Real-IP         $remote_addr;
        proxy_set_header   Host              $http_host;
        proxy_set_header   X-Forwarded-For   $proxy_add_x_forwarded_for;
    }
		
    # redis监控 一般用不着
    location ^~ /redis-stat/ {
        proxy_pass http://127.0.0.1:63790/;
    }

    error_page 404 /404.html;
    location = /40x.html {
    }

    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
    }
}
```

## 添加新页面

1. 添加菜单

   菜单管理添加菜单,并且让用户获取到菜单，如`userList`做`menu_key`

2. 新增 vue 文件 

   在 `src/views` 下新建页面的`vue`文件，如果相关页面有多个，可以新建一个文件夹来放置相关文件。

3. 将文件加入菜单和路由

   这一步可能是在菜单配置里操作，那么在后台或者数据库操作也行，如果不想放在数据库，可以按下面的步骤

   后端添加好菜单并确保能获取到你新加的路由，在`src\utils\routerUtil.js`中*动态引入页面组件*,如:

   ```javascript
   userList: () => import('@/views/system/UserList')
   ```

   加好后，访问 `http://localhost:8000/system/userList` 就可以看到新增的页面了。

   `system`是上级模块名称`userList`是当前模块名称，也就是数据库中的`menu_key`(唯一)，根据`children`关系获得 

4. 新增 model （非必须）

   布局及路由都配置好之后，回到之前新建的 `newPage.vue`，可以开始写业务代码了！如果需要用到 `vuex` 中的数据流，还需要在 `src/store/model` 中建立相应的 model。

5. 书写业务接口

   在已有微服务中添加接口或新建一个微服务(`gateway`中配置好映射)，添加完成可以用`postman`(推荐)测试接口是否正常，在`src/api`中添加对应请求方法,参考`system.js`,在`vue`文件中导入

   

   

   

