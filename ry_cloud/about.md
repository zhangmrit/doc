## 项目简介
`springcloud`版本的若依,集成`springcloud`的几大神兽:`eureka`,`gateway`,`hystrix`,`feign`,`config`...自定义`token`实现授权，直接存到`redis`,集成工作流`Activiti`

前端UI框架为`ant design vue`,脚手架为`ant design vue pro`

**演示系统**：http://ruoyi.ant.zmrit.com

**启动视频**：https://www.bilibili.com/video/av95044508/

**后端源码**：https://gitee.com/zhangmrit/ruoyi-cloud

**前端源码**：https://gitee.com/zhangmrit/ruoyi-ant

**antd vue文档**：https://vue.ant.design/docs/vue/introduce-cn/

**我的博客**：http://zmrit.com

**架构师小站**： http://club.zmrit.com

**QQ群**：[![加入QQ群](https://img.shields.io/badge/755109875-blue.svg)](https://jq.qq.com/?_wv=1027&k=5JGXHPD)  点击按钮入群。

**spring-cloud版本**：`Hoxton.SR3`

**项目合作进群细聊**

## 分支说明

- **master** spring原生方式，使用`eureka`做注册中心和`spring config`做配置中心
- **nacos** 集成`spring-cloud-alibaba` 使用`nacos`做注册中心和配置中心



本项目FORK自  [若依/RuoYi](https://gitee.com/y_project/RuoYi)

蓝本是[zhangmrit/Ruoyi](https://gitee.com/zhangmrit/RuoYi)

为了让更多的人受众，以后会在`github`更新，`gitee`将作为国内加速备用地址（不一定同步更新）：

后端源码：https://gitee.com/zhangmrit/ruoyi-cloud

前端源码：https://gitee.com/zhangmrit/ruoyi-ant

## 项目结构

```
ruoyi-cloud
|
├──ruoyi-common --通用包
|  |
|  ├──ruoyi-common-core --核心工具包
|  |
|  ├──ruoyi-common-redis --redis工具包
|  |
|  ├──ruoyi-common-log --日志工具包
|  |
|  ├──ruoyi-common-auth --权限工具包
|  |
|  ├──ruoyi-common-swagger --api文档
|
├──ruoyi-config --cloud统一配置中心 nacos分支移除
|
├──ruoyi-eureka --注册中心 nacos分支移除
|
├──ruoyi-gateway --网关
|
├──ruoyi-service-api --服务api模块
|  |
|  ├──ruoyi-system-api --系统业务api
|
├──ruoyi-service --微服务
|  |
|  ├──ruoyi-system --系统业务
|  |
|  ├──ruoyi-auth --授权中心
|  |
|  ├──ruoyi-gen --代码生成
|  |
|  ├──ruoyi-dfs --文件系统
|  |
|  ├──ruoyi-activiti --工作流引擎和业务
|
├──ruoyi-tool --工具
|  |
|  ├──ruoyi-monitor --监控中心 非必须
|
├──ruoyi-ant --前端 使用ant design框架
```



## 更新日志

- 1.1.0-SNAPSHOT 2020-03-12
  - 根据流程文件部署流程定义
  - fix:用户管理调用所有部门接口
  - 工作流程追踪高亮连线
  - 工作流高亮已执行环节
  - 增加工作流`activiti`支持
  - fix:更新`path`等为空时无效
  - `menu`增加路径、重定向、隐藏等字段
  - 增加捐赠内容板块
  - fix:代码生成调优
  - fix: gen bugs gitee [!9](https://gitee.com/zhangmrit/ruoyi-cloud/pulls/9)
  - 动态菜单优化

- 1.0.2-SNAPSHOT 2019-09-30
  - 优化角色授权实现联动和半选
  - 用户管理增加部门检索功能
  - 代码生成增加树形模板
  - 代码生成模板适配`vue`
  - 分布式文件系统
  - 修复代码生成前缀无法自动去除
  - 操作日志增加请求方法
  - 移除`jwt`依赖
  - `swagger`接口文档支持
  - 前端`i18n`国际化支持
  - `vue-cropper`头像组件
- 1.0.1-SNAPSHOT 2019-08-23
  
  - 新增`ruoyi 4.0`代码生成功能，**未修改模板**
  - 修复数据权限导致不能分页的bug
  - 增加`oss`文件上传
  - 增加参数配置管理
  - 升级`spring-alibaba-cloud`版本到`2.1.x`
  - 优化读写分离逻辑
- 1.0.0-SNAPSHOT 2019-07-25
  
  - maven坐标修改为`com.ruoyi.cloud`
  - 升级`springboot`版本到`2.1.6`
  - 升级`springcloud`版本到`Greenwich.SR2`
  - 用户管理部门不应该选中父节点
  - 菜单授权联动bug（受限于`ant-design`）,取消父子联动
  - 数据权限
  - 移除多余的依赖声明
  - 增加`vue`代码生成（部分）
  - 修复登陆记录调取`token`的bug