# 分布式配置中心

微服务意味着要将单体应用中的业务拆分成一个个子服务，每个服务的粒度相对较小，因此系统中会出现大量的服务。由于每个服务都需要必要的配置信息才能运行，所以一套集中式的、动态的配置管理设施是必不可少的。SpringCloud提供了ConfigServer来解决这个问题，我们每一个微服务自己带着一个`application.yml`，上百个配置文件的管理....../(ㄒoㄒ)/~~

## 是什么

SpringCloud Config为微服务架构中的微服务提供集中化的外部配置支持，配置服务器为各个不同微服务应用的所有环境提供了一个中心化的外部配置。

## 能干嘛

- 集中管理配置文件
- 不同环境不同配置，动态化的配置更新，分环境部署比如dev/test/prod/beta/release
- 运行期间动态调整配置，不再需要在每个服务部署的机器上编写配置文件，服务会向配置中心统一拉取配置自己的信息
- 当配置发生变动时，服务不需要重启即可感知到配置的变化并应用新的配置
- 将配置信息以REST接口的形式暴露

## 配置存储

由于SpringCloud Config默认使用Git来存储配置文件(也有其它方式,比如支持SVN和本地文件)，
但最推荐的还是Git，而且使用的是http/https访问的形式，如Github、Gitee，开发的时候可以选择本地模式方便调试

在github新建一个仓库cloud-zero-config

上传配置,**请保存为UTF-8格式**

```yaml
spring:
  profiles:
    active: dev
---
spring:
  profiles: dev     #开发环境
  application: 
    name: cloud-zero-config-dev
---
spring:
  profiles: test   #测试环境
  application: 
    name: cloud-zero-config-test
#  请保存为UTF-8格式
 
```

## 怎么用

新建模块 cloud-zero-config

pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>cloud-zero</artifactId>
        <groupId>com.wind</groupId>
        <version>1.0.0</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>cloud-zero-config</artifactId>

    <dependencies>
        <!--配置中心 -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-config-server</artifactId>
        </dependency>
        <!--web 模块 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <exclusions>
                <!--排除tomcat依赖 -->
                <exclusion>
                    <artifactId>spring-boot-starter-tomcat</artifactId>
                    <groupId>org.springframework.boot</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <!--undertow容器 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-undertow</artifactId>
        </dependency>
        <!-- spring-boot-devtools -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <optional>true</optional> <!-- 表示依赖不会传递 -->
        </dependency>
    </dependencies>


    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <fork>true</fork> <!-- 如果没有该配置，devtools不会生效 -->
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
```

application.yml

```yaml
server:
  port: 8888

spring:
  application:
    name: cloud-zero-config
  # 配置中心
  cloud:
    config:
      server:
        git:
          # 修改成你自己的git仓库
          uri: https://github.com/zhangmrit/cloud-zero-config.git

```

Git仓库如果是私有仓库需要填写用户名密码，示例是公开仓库，所以不配置密码。

ConfigApp

```java
package com.wind.cloud.zero.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


@SpringBootApplication
@EnableConfigServer
public class ConfigApp
{
    public static void main(String[] args)
    {
        SpringApplication.run(ConfigApp.class);
    }
}

```

测试

> http://127.0.0.1:8888/application-dev.yml
>
> http://127.0.0.1:8888/application-test.yml

**读取规则**

>① /{application}/{profile}[/{label}]
>
>② /{application}-{profile}.yml
>
>③ /{label}/{application}-{profile}.yml
>
>④ /{application}-{profile}.properties
>
>⑤ /{label}/{application}-{profile}.properties

http://127.0.0.1:8888/application/test/master 对应① mster是仓库分支名

http://127.0.0.1:8888/application-dev.yml 对应② 默认分支

http://127.0.0.1:8888/master/application-dev.yml 对应③

## 整合eureka

config注册到eureka，eurek**不从**config获取配置，其他微服务从eureka连接config服务并获取配置

将之前多环境的配置内容上传到git仓库，文件名：cloud-zero-eureka.yml

config serve添加依赖

```xml
        <!--eureka 客户端 -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
```

config serve的yml

```yml
server:
  port: 8888

spring:
  application:
    name: cloud-zero-config
  # 配置中心
  cloud:
    config:
      server:
        git:
          # 修改成你自己的git仓库
          uri: https://github.com/zhangmrit/cloud-zero-config.git

# 注册中心配置
eureka:
  client: #客户端注册进eureka服务列表内
    service-url:
      defaultZone: http://eureka7001.com:7001/eureka
      #defaultZone: http://eureka7001.com:7001/eureka/,http://eureka7002.com:7002/eureka/,http://eureka7003.com:7003/eureka/
  instance:
    instance-id: ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port}
    prefer-ip-address: true     #访问路径可以显示IP地址

# 暴露监控端点
management:
  endpoints:
    web:
      exposure:
        include: '*'
```

新建模块 cloud-zero-config-client

pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>cloud-zero</artifactId>
        <groupId>com.wind</groupId>
        <version>1.0.0</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>cloud-zero-config-client</artifactId>
    <dependencies>
        <!--配置中心 -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-config-client</artifactId>
        </dependency>
        <!--eureka 客户端 -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <!--web 模块 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <exclusions>
                <!--排除tomcat依赖 -->
                <exclusion>
                    <artifactId>spring-boot-starter-tomcat</artifactId>
                    <groupId>org.springframework.boot</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <!--undertow容器 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-undertow</artifactId>
        </dependency>
        <!-- spring-boot-devtools -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <optional>true</optional> <!-- 表示依赖不会传递 -->
        </dependency>
    </dependencies>


    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <fork>true</fork> <!-- 如果没有该配置，devtools不会生效 -->
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
```

application.yml

```yaml
server:
  port: 8006

spring:
  application:
    name: cloud-zero-config-client
  cloud:
    config:
      fail-fast: true
      name: ${spring.application.name}
      profile: ${spring.profiles.active}
      discovery:
        enabled: true
        service-id: cloud-zero-config
  profiles:
    active: dev
  devtools:
    restart:
      enabled: true

eureka:
  client: #客户端注册进eureka服务列表内
    service-url:
      defaultZone: http://eureka7001.com:7001/eureka
      #defaultZone: http://eureka7001.com:7001/eureka/,http://eureka7002.com:7002/eureka/,http://eureka7003.com:7003/eureka/
  instance:
    instance-id: ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port}
    prefer-ip-address: true     #访问路径可以显示IP地址
# 暴露监控端点
management:
  endpoints:
    web:
      exposure:
        include: '*'
```

ConfigClientApp

```java
package com.wind.cloud.zero.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ConfigClientApp
{
    public static void main(String[] args)
    {
        SpringApplication.run(ConfigClientApp.class);
    }
}
```

TestController

```java
package com.wind.cloud.zero.config.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController
{
    @Value("${config.version}")
    private String version;

    @GetMapping("/test")
    public String test()
    {
        return version;
    }
}

```

上传cloud-zero-config-client.yml到git仓库

```yaml
spring:
  profiles:
    active: dev
---
spring:
  profiles: dev

config:
  version: dev_version

---
spring:
  profiles: test

config:
  version: test_version

---
spring:
  profiles: prod

config:
  version: prod_version
```

先启动eureka，再启动config-serve,最后启动config-client

访问 http://127.0.0.1:8888/cloud-zero-config-client-dev.yml

会发现有些内容不属于我们，怎么回事?

读取顺序

>（1）首先根据application的名称来寻找，即application的名字（比如，即项目的名称abc）.*来查找文件。
>
>（2）如果没有对应的名字，则读取application.properties里面的内容，如果这个也没有，则返回错误。
>
>比如git仓库中的master分支上面有两个配置文件：foobar-dev.yml和application.yml。
>
>这两个文件中都有一个配置，名称相同，但是内容不同，假设foobar-dev.yml中的是profile=abc-foobar，application.yml中的是profile=abc-application
>
>我们访问的时候比如：localhost:8080/master/foobar-dev.yml，则命中到foobar-dev.yml，读取的是abc-foobar
>
>如果访问localhost:8080/master/foobar-default.yml，这个文件在git中是不存在的，则命中到application.yml中，读取的是profile=abc-application
>
>总结：这里有个 优先级，先找对应名称的，如果没有则找默认的application.yml的内容，如果再没有，那就没办法了。

所以最好不要保存application.yml，用实际微服务名称来命名

测试 

> 127.0.0.1:8006/test

本节源码 https://gitee.com/zhangmrit/cloud-zero/tree/config