# 展望

- SpringCloud Stream：数据流操作开发包
- SpringCloud Turbine是聚合服务器发送事件流数据的一个工具，用来监控集群下hystrix的metrics情况。
- SpringCloud Task：提供云端计划任务管理、任务调度。
- SpringCloud Sleuth：日志收集工具包实现了一种分布式追踪解决方案，封装了Dapper和log-based追踪以及Zipkin和HTrace操作
- SpringCloud Security：基于spring security的安全工具包，为应用程序添加安全控制
- 服务部署：Kubernetes ， OpenStack
- 全链路追踪：Zipkin，brave
- 服务监控：zabbix
- SpringCloud CLI：基于 Spring Boot CLI，可以让你以命令行方式快速建立云组件。
- 全局控制：选举leader、全局锁、全局唯一id
- 安全鉴权： auth2、 openId connect
- 自动化构建与部署： gitlab + jenkins + docker 
- 服务监控和告警（Spring Boot Admin）

...