# Rest服务起手
## 初始化

新建文件夹 cloud-zero

添加`pom.xml`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.wind</groupId>
    <artifactId>cloud-zero</artifactId>
    <packaging>pom</packaging>
    <version>1.0.0</version>


    <properties>
        <java.version>1.8</java.version>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <spring-boot.version>2.1.7.RELEASE</spring-boot.version>
        <spring-cloud.version>Greenwich.SR2</spring-cloud.version>
        <mybatis.spring.version>1.3.2</mybatis.spring.version>
        <lombok.version>1.18.4</lombok.version>
    </properties>


    <dependencyManagement>
        <dependencies>
            <!-- spring cloud -->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!-- SpringBoot的依赖配置 -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.mybatis.spring.boot</groupId>
                <artifactId>mybatis-spring-boot-starter</artifactId>
                <version>${mybatis.spring.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <!--子模块可以自动继承-->
    <dependencies>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>

    </build>

</project>
```

## 构建模块

`springcloud`的单体就是`springboot`，那么先从一个`springboot`项目开始

### cloud-zero-api 
封装的一些模型和公共配置

  ```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>cloud-zero</artifactId>
        <groupId>com.wind</groupId>
        <version>1.0.0</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>cloud-zero-api</artifactId>
    <packaging>jar</packaging>

</project>
  ```

  

### cloud-zero-provider-user
微服务落地提供者 用户

  - pom.xml

  ```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>cloud-zero</artifactId>
        <groupId>com.wind</groupId>
        <version>1.0.0</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>cloud-zero-provider-user</artifactId>
    <packaging>jar</packaging>
    <dependencies>
        <dependency>
            <groupId>com.wind</groupId>
            <artifactId>cloud-zero-api</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
        </dependency>
    </dependencies>
</project>
  ```

  - application.yml

  ```yaml
server:
  port: 8001

spring:
  application:
    name: cloud-zero-user
  datasource:
    driverClassName: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/cloud-zero?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8
    username: root
    password: root


mybatis:
  type-aliases-package: com.wind.cloud.demo.user.domain    # 所有Entity别名类所在包
  mapper-locations:
    - classpath:mapper/*.xml                            # mapper映射文件

  ```

  - 新建User.java,UserMapper.java,UserMapper.xml,我这里用mybatiscodehelper直接生成了
  - UserController.java

  ```java
package com.wind.cloud.zero.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wind.cloud.zero.domain.User;
import com.wind.cloud.zero.mapper.UserMapper;

@RestController
@RequestMapping("user")
public class UserController
{
    @Autowired
    private UserMapper userMapper;

    @GetMapping("list")
    public List<User> list()
    {
        return userMapper.findByAll(null);
    }

    @GetMapping("get/{id}")
    public User get(@PathVariable Integer id)
    {
        return userMapper.selectByPrimaryKey(id);
    }

    @GetMapping("del/{id}")
    public boolean del(@PathVariable Integer id)
    {
        return userMapper.deleteByPrimaryKey(id) > 0;
    }

    @PostMapping("add")
    public boolean add(@RequestBody User user)
    {
        return userMapper.insert(user) > 0;
    }
}
  ```

  - UserProviderApp

  ```java
package com.wind.cloud.zero;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.wind.cloud.zero")
public class UserProviderApp
{
    public static void main(String[] args)
    {
        SpringApplication.run(UserProviderApp.class);
    }
}
  ```

  



上面就是rest服务的提供者的一个构建，熟练的话几分钟就完成了，用postman测试接口都是否正常就可以进行下一步了

本节源码 https://gitee.com/zhangmrit/cloud-zero/tree/rest/