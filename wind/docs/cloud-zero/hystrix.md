# Hystrix熔断器

## 是什么

Hystrix [hɪst'rɪks]，中文含义是豪猪，因其背上长满棘刺，从而拥有了自我保护的能力。本文所说的Hystrix是Netflix开源的一款容错框架，同样具有自我保护能力。

Hystrix是一个用于处理分布式系统的延迟和容错的开源库，在分布式系统里，许多依赖不可避免的会调用失败，比如超时、异常等，Hystrix能够保证在一个依赖出问题的情况下，不会导致整体服务失败，避免级联故障，以提高分布式系统的弹性。

“断路器”本身是一种开关装置，当某个服务单元发生故障之后，通过断路器的故障监控（类似熔断保险丝），向调用方返回一个符合预期的、可处理的备选响应（FallBack），而不是长时间的等待或者抛出调用方无法处理的异常，这样就保证了服务调用方的线程不会被长时间、不必要地占用，从而避免了故障在分布式系统中的蔓延，乃至雪崩。

## 能干嘛

复杂分布式体系结构中的应用程序有许多依赖项，每个依赖项在某些时候都不可避免地会失败。如果主机应用程序没有与这些外部故障隔离，那么它有可能被他们拖垮。

例如，对于一个依赖于30个服务的应用程序，每个服务都有99.99%的正常运行时间，你可以期望如下：

​	99.9930  =  99.7% 可用

​	也就是说一亿个请求的0.03% = 3000000 会失败

​	如果一切正常，那么每个月有2个小时服务是不可用的

现实通常是更糟糕

------

当一切正常时，请求看起来是这样的：

![](https://github.com/Netflix/Hystrix/wiki/images/soa-1-640.png)

当其中有一个系统有延迟时，它可能阻塞整个用户请求：

![](https://github.com/Netflix/Hystrix/wiki/images/soa-2-640.png)

在高流量的情况下，一个后端依赖项的延迟可能导致所有服务器上的所有资源在数秒内饱和（PS：意味着后续再有请求将无法立即提供服务）

![](https://github.com/Netflix/Hystrix/wiki/images/soa-3-640.png)

## Hystrix设计原则是什么

- 防止任何单个依赖项耗尽所有容器（如Tomcat）用户线程。
- 甩掉包袱，快速失败而不是排队。
- 在任何可行的地方提供回退，以保护用户不受失败的影响。
- 使用隔离技术（如隔离板、泳道和断路器模式）来限制任何一个依赖项的影响。
- 通过近实时的度量、监视和警报来优化发现时间。
- 通过配置的低延迟传播来优化恢复时间。
- 支持对Hystrix的大多数方面的动态属性更改，允许使用低延迟反馈循环进行实时操作修改。
- 避免在整个依赖客户端执行中出现故障，而不仅仅是在网络流量中。

## Hystrix是如何实现它的目标的

1. 用一个HystrixCommand 或者 HystrixObservableCommand （这是命令模式的一个例子）包装所有的对外部系统（或者依赖）的调用，典型地它们在一个单独的线程中执行
2. 调用超时时间比你自己定义的阈值要长。有一个默认值，对于大多数的依赖项你是可以自定义超时时间的。
3. 为每个依赖项维护一个小的线程池(或信号量)；如果线程池满了，那么该依赖性将会立即拒绝请求，而不是排队。
4. 调用的结果有这么几种：成功、失败（客户端抛出异常）、超时、拒绝。
5. 在一段时间内，如果服务的错误百分比超过了一个阈值，就会触发一个断路器来停止对特定服务的所有请求，无论是手动的还是自动的。
6. 当请求失败、被拒绝、超时或短路时，执行回退逻辑。
7. 近实时监控指标和配置变化。

当你使用Hystrix来包装每个依赖项时，上图中所示的架构会发生变化，如下图所示：

每个依赖项相互隔离，当延迟发生时，它会被限制在资源中，并包含回退逻辑，该逻辑决定在依赖项中发生任何类型的故障时应作出何种响应：

![](https://github.com/Netflix/Hystrix/wiki/images/soa-4-isolation-640.png)

以上图片均来自hystrix官方文档 https://github.com/Netflix/Hystrix/wiki

## 怎么用

### 服务熔断

就像家里的保险丝一样，顾名思义，服务不可用以后返回你指定的内容

添加依赖

```xml
		<!--hystrix-->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
        </dependency>
```

给User对象添加注解，实现链式调用

```java
@Accessors(chain = true)
```

一旦调用服务方法失败并抛出了错误信息后，会自动调用`@HystrixCommand`标注好的`fallbackMethod`调用类中的指定方法

UserController改造一下

```java
    @GetMapping("get/{id}")
    @HystrixCommand(fallbackMethod = "processHystrix_Get")
    public User get(@PathVariable Integer id)
    {
        User user = userMapper.selectByPrimaryKey(id);
        if (null == user)
        {
            throw new RuntimeException("找不到用户");
        }
        return user;
    }
    
    public User processHystrix_Get(@PathVariable Integer id)
    {
        return new User().setId(id).setName("该ID：" + id + " 没有对应的信息,null").setAge(0);
    }
```

UserProviderApp修改为UserProviderHystrixApp

添加注解

```java
@EnableCircuitBreaker // 对hystrixR熔断机制的支持
```

测试

- 启动eurke
- 启动UserProviderHystrixApp
- 启动consumer

### 服务降级

整体资源快不够了，忍痛将某些服务先关掉，待渡过难关，再开启回来。

服务降级处理是在客户端实现完成的，与服务端没有关系

在api中新建Fallback类

```java
package com.wind.cloud.zero.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.wind.cloud.zero.domain.User;
import com.wind.cloud.zero.feign.RemoteUserService;

import feign.hystrix.FallbackFactory;

@Component
public class RemoteUserFacllbackFactory implements FallbackFactory<RemoteUserService>
{
    @Override
    public RemoteUserService create(Throwable throwable)
    {
        return new RemoteUserService()
        {
            @Override
            public User get(Integer id)
            {
                return null;// 注意这里故意区分之前的返回内容，返回null
            }

            @Override
            public List<User> list()
            {
                return null;
            }
        };
    }
}

```

修改RemoteUserService注解

```java
@FeignClient(value = "CLOUD-ZERO-USER", fallbackFactory = RemoteUserFacllbackFactory.class)
```

consumer修改yml

```yaml
feign:
  hystrix:
    enabled: true
```

也是正常启动1个eureka,2个prvider，1个consumer测试轮询效果，feign自带负载均衡，可以故意关闭一个provider测试

## 监控台

新建模块`cloud-zero-hystrix-dashboard`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>cloud-zero</artifactId>
        <groupId>com.wind</groupId>
        <version>1.0.0</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>cloud-demo-hystrix-dashboard</artifactId>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!--eureka 客户端 -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
        <!-- actuator监控信息完善 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-hystrix-dashboard</artifactId>
        </dependency>
    </dependencies>
    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <configuration>
                    <delimiters>@</delimiters>
                    <useDefaultDelimiters>false</useDefaultDelimiters>
                </configuration>
            </plugin>
        </plugins>

    </build>
</project>
```

HystrixDashboardApp

```java
package com.wind.cloud.zero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
@EnableHystrixDashboard
public class HystrixDashboardApp
{
    public static void main(String[] args)
    {
        SpringApplication.run(HystrixDashboardApp.class);
    }
}

```

提供者application.yml添加，否则报404

```yaml
management:
  endpoints:
    web:
      exposure:
        include: '*'
```

监控application.yml

```yaml
spring:
  application:
    name: cloud-zero-hystrix-dashboard

eureka:
  client: # 客户端注册进eureka服务列表内
    service-url:
      defaultZone: http://localhost:7001/eureka
      #defaultZone: http://eureka7001.com:7001/eureka/,http://eureka7002.com:7002/eureka/,http://eureka7003.com:7003/eureka/
  instance:
    instance-id: ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port}
    prefer-ip-address: true     # 访问路径可以显示IP地址

management:
  endpoints:
    web:
      exposure:
        include: '*'


info:
  app.name: cloud-zero
  company.name: www.zmrit.com
  build.artifactId: @project.artifactId@
  build.version: @project.version@

server:
  port: 9050

```

注意，provider要使用hystrix才能看到效果（要不然都是ping），具体功能不再详述，有时间以后再开一个课程

本节源码

- hystrix https://gitee.com/zhangmrit/cloud-zero/tree/hystrix
- hystrix-dashboard https://gitee.com/zhangmrit/cloud-zero/tree/hystrix-dashboard