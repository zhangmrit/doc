# Ribbon负载均衡

## 是什么

Spring Cloud Ribbon是基于Netflix Ribbon实现的一套客户端**负载均衡**的工具。

简单的说，Ribbon是Netflix发布的开源项目，主要功能是提供客户端的软件负载均衡算法，将Netflix的中间层服务连接在一起。Ribbon客户端组件提供一系列完善的配置项如连接超时，重试等。简单的说，就是在配置文件中列出Load Balancer（简称LB）后面所有的机器，Ribbon会自动的帮助你基于某种规则（如简单轮询，随机连接等）去连接这些机器。我们也很容易使用Ribbon实现自定义的负载均衡算法。

## 能干嘛

LB，即负载均衡(Load Balance)，在微服务或分布式集群中经常用的一种应用。
负载均衡简单的说就是将用户的请求平摊的分配到多个服务上，从而达到系统的HA。
常见的负载均衡有软件Nginx，LVS，硬件 F5等。
相应的在中间件，例如：dubbo和SpringCloud中均给我们提供了负载均衡，SpringCloud的负载均衡算法可以自定义。 

### 集中式LB

即在服务的消费方和提供方之间使用独立的LB设施(可以是硬件，如F5, 也可以是软件，如nginx), 由该设施负责把访问请求通过某种策略转发至服务的提供方；

### 进程内LB

将LB逻辑集成到消费方，消费方从服务注册中心获知有哪些地址可用，然后自己再从这些地址中选择出一个合适的服务器。

Ribbon就属于进程内LB，它只是一个类库，集成于消费方进程，消费方通过它来获取到服务提供方的地址。

## 怎么用

###### 消费者

consumer-user

修改pom.xml

```xml
<!-- Ribbon相关 -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
</dependency>
```

对ConfigBean进行新注解`@LoadBalanced` ,获得Rest时加入Ribbon的配置

```java
@Configuration
public class ConfigBean
{
  @Bean
  @LoadBalanced // Spring Cloud Ribbon是基于Netflix Ribbon实现的一套客户端 负载均衡的工具。
  public RestTemplate getRestTemplate()
  {
   return new RestTemplate();
  }
}
```

UserController

Ribbon和Eureka整合后Consumer可以直接调用服务而不用再关心地址和端口号

```java
    // private static final String REST_URL_PREFIX = "http://localhost:8001";
    private static final String REST_URL_PREFIX = "http://CLOUD-ZERO-USER";
```

###### 提供者

克隆数据库`cloud-zero2`,给数据做标记

application.yml

```yaml
spring:
  application:
    name: cloud-zero-user
  datasource:
    driverClassName: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/cloud-zero?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8
    username: root
    password: root
  profiles:
    active: 8001

mybatis:
  type-aliases-package: com.wind.cloud.demo.user.domain    # 所有Entity别名类所在包
  mapper-locations:
    - classpath:mapper/*.xml                       # mapper映射文件

eureka:
  client: # 客户端注册进eureka服务列表内
    service-url:
      defaultZone: http://localhost:7001/eureka
      #defaultZone: http://eureka7001.com:7001/eureka/,http://eureka7002.com:7002/eureka/,http://eureka7003.com:7003/eureka/
  instance:
    instance-id: ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port}
    prefer-ip-address: true     # 访问路径可以显示IP地址


info:
  app.name: cloud-zero
  company.name: www.zmrit.com
  build.artifactId: @project.artifactId@
  build.version: @project.version@

---
spring:
  profiles: 8001
server:
  port: 8001
---
spring:
  profiles: 8002
  datasource:
    url: jdbc:mysql://localhost:3306/cloud-zero2?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8
server:
  port: 8002



```

启动一个`eureka`,2个`provider`,1个`consumer`

访问`127.0.0.1/user/get/1`查看返回结果

Ribbon的默认轮询规则是轮询，可以手动改造

Ribbon其实就是一个软负载均衡的客户端组件，
他可以和其他所需请求的客户端结合使用，和eureka结合只是其中的一个实例

## IRule

IRule：根据特定算法中从服务列表中选取一个要访问的服务

- RoundRobinRule 轮询

- RandomRule 随机

- AvailabilityFilteringRule

  会先过滤掉由于多次访问故障而处于断路器跳闸状态的服务，
  还有并发的连接数量超过阈值的服务，然后对剩余的服务列表按照轮询策略进行访问

- WeightedResponseTimeRule

  根据平均响应时间计算所有服务的权重，响应时间越快服务权重越大被选中的概率越高。
  刚启动时如果统计信息不足，则使用RoundRobinRule策略，等统计信息足够，
  会切换到WeightedResponseTimeRule

- RetryRule

  先按照RoundRobinRule的策略获取服务，如果获取服务失败则在指定时间内会进行重试，获取可用的服务

- BestAvailableRule

  会先过滤掉由于多次访问故障而处于断路器跳闸状态的服务，然后选择一个并发量最小的服务

- ZoneAvoidanceRule

  默认规则,复合判断server所在区域的性能和server的可用性选择服务器

## 自定义

消费者模块新建`MySelfRule`

```java
package com.wind.cloud.rule;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
 
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
 
@Configuration
public class MySelfRule
{
  @Bean
  public IRule myRule()
  {
   return new RandomRule();//Ribbon默认是轮询，我自定义为随机
  }
}
```

官方文档明确给出了警告：
这个自定义配置类不能放在`@ComponentScan`所扫描的当前包下以及子包下，
否则我们自定义的这个配置类就会被所有的Ribbon客户端所共享，也就是说
我们达不到特殊化定制的目的了。

启动类添加`@RibbonClient`

在启动该微服务的时候就能去加载我们的自定义Ribbon配置类，从而使配置生效，形如：

```java
@RibbonClient(name="CLOUD-ZERO-USER",configuration= MySelfRule.class)
```

测试效果

## 深度自定义

参考源码 https://github.com/Netflix/ribbon/blob/master/ribbon-loadbalancer/src/main/java/com/netflix/loadbalancer/RandomRule.java

新建自定义规则类

```java
package com.wind.cloud.rule;

import java.util.List;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;

public class MyRandomRule extends AbstractLoadBalancerRule
{
    // total = 0 // 当total==5以后，我们指针才能往下走，
    // index = 0 // 当前对外提供服务的服务器地址，
    // total需要重新置为零，但是已经达到过一个5次，我们的index = 1
    // 分析：我们5次，但是微服务只有8001 8002 两台，OK？
    //
    private int total        = 0; // 总共被调用的次数，目前要求每台被调用5次

    private int currentIndex = 0; // 当前提供服务的机器号

    public Server choose(ILoadBalancer lb, Object key)
    {
        if (lb == null)
        {
            return null;
        }
        Server server = null;
        while (server == null)
        {
            if (Thread.interrupted())
            {
                return null;
            }
            List<Server> upList = lb.getReachableServers();
            List<Server> allList = lb.getAllServers();
            int serverCount = allList.size();
            if (serverCount == 0)
            {
                return null;
            }
            if (total < 5)
            {
                server = upList.get(currentIndex);
                total++;
            }
            else
            {
                total = 0;
                currentIndex++;
                if (currentIndex >= upList.size())
                {
                    currentIndex = 0;
                }
            }
            if (server == null)
            {
                Thread.yield();
                continue;
            }
            if (server.isAlive())
            {
                return (server);
            }
            server = null;
            Thread.yield();
        }
        return server;
    }

    @Override
    public Server choose(Object key)
    {
        return choose(getLoadBalancer(), key);
    }

    @Override
    public void initWithNiwsConfig(IClientConfig clientConfig)
    {
    }
}

```

MySelfRule

```java
return new MyRandomRule();//我自定义为每个机器被访问5次
```

启动消费者查看效果

本节源码 https://gitee.com/zhangmrit/cloud-zero/tree/ribbon/