module.exports = {
    title: 'wind',
    description: '架构师小站',
    head: [ // 注入到当前页面的 HTML <head> 中的标签
        //['link', { rel: 'icon', href: '/logo.jpg' }], // 增加一个自定义的 favicon(网页标签的图标)
		['link', { rel: 'shortcut icon', type: "image/x-icon", href: `/favicon.ico` }]
    ],
    base: '/', // 这是部署到github相关的配置
    markdown: {
        lineNumbers: false // 代码块显示行号
    },
	plugins: [
		['@vuepress/back-to-top'],
		['@dovyp/vuepress-plugin-clipboard-copy'],
		['copyright', {
			noCopy: true,   // 选中的文字将无法被复制
			minLength: 100, // 如果长度超过 100 个字符
			// 默认情况下禁用这个插件
			// 你可以在 frontmatter 中激活它
			disabled: true,
			// 激活时所有文字将无法被选中
			noSelect: true,
		}],
	],
    themeConfig: {
        nav: [
            { text: '主页', link: '/' },
            { text: '指南', link: '/guide/' },
            { text: '从零开始', link: '/cloud-zero/' },
            { text: 'RuoYi Cloud', link: 'http://doc.rycloud.zmrit.com/' },
            // 下拉列表的配置
            {
                text: 'Git',
                items: [
                { text: 'Github', link: 'https://github.com/zhangmrit/' },
                { text: 'Gitee', link: 'https://gitee.com/zhangmrit/' }
                ]
            }
        ],
        
        sidebar: {
            '/guide/':[
                {
                    title: '指南',
                    collapsable: false,
                    children: [
                        '',
                        'cloud-zero',
                        'ruoyi-cloud'
                    ]
                }
            ],
            '/cloud-zero/':[
                {
                    title: '从零开始',
                    collapsable: false,
                    children: [
                        '',
                        'about',
                        'springcloud',
                        'rest',
                        'eureka',
                        'resttemplate',
                        'ribbon',
                        'feign',
                        'hystrix',
						'gateway',
                        'config',
                        'nacos',
						'todo'
                    ]
                }
            ],
        },
        searchMaxSuggestions: 10,
        lastUpdated: 'Last Updated'
    }
  }